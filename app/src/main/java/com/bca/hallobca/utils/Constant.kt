package com.bca.hallobca.utils

class Constant {
        companion object Network {
                const val ROLE_GUEST = "role"
                const val ROLE_USER = "user"

                const val BASE_URL = "http://202.6.215.216:8181/rest/"
                const val API_GET_NAME = "searchCIS/getName"
                const val API_GET_EMAIL = "searchCIS/getEmail"
                const val API_CHECK_ACC = "searchCIS/checkCardNumber"
                const val API_GET_PHONE = "InquiryPhoneBanking/getPhoneNumberList"
                const val API_GEN_OTP = "OTP/generateOTP"
                const val API_VAL_OTP = "OTP/verifyOTP"

                const val API_SEARCH_BY_UNDEFINED = ""
                const val API_SEARCH_BY_ACCOUNT = "DepositAccountNumber"
                const val API_SEARCH_BY_DEBIT_CARD = "DebitCardNumber"
                const val API_SEARCH_BY_CREDIT_CARD = "CCCardNumber"

                val SEARCH_BY_LIST = arrayOf(
                        API_SEARCH_BY_UNDEFINED,
                        API_SEARCH_BY_ACCOUNT,
                        API_SEARCH_BY_DEBIT_CARD,
                        API_SEARCH_BY_CREDIT_CARD
                )

                const val CRTYPE_ACC = 1
                const val CRTYPE_DEBIT = 2
                const val CRTYPE_CREDIT = 3

                const val PHONE_NUMBER = "nomor_hp"

                const val OTP_RESEND_TIME = "otp_resend_time"

                const val SP_GLOBAL = "com.bca.hallobca.prefs"

                const val DISCLAIMER_AGREE = "disclaimer_agree"

                const val CR_TYPE = "cred_type"
                const val CR_VAL = "cred_value"
                const val EPIN = "enc_pin"
                const val ROLE_STATUS = "registered"
                const val EMAIL = "email"
                const val UNAME = "username"
                const val ABSPP = "absolute_pic_path"

                const val CALL_DEST = "call_destination"

                const val SEL_CR_TYPE = "Pilih Jenis Akun"
                const val CR_TYPE_1 = "Nomor Rekening"
                const val CR_TYPE_2 = "Kartu Debit"
                const val CR_TYPE_3 = "Kartu Kredit"

                const val PERMISSION_REQ_CODE_CHAT = 332
                const val PERMISSION_REQ_CODE_CAM = 333
                const val REQUEST_IMAGE_CAPTURE = 334

                //avaya const
                const val MAX_CONTENT_ID_LENGTH = 32

                const val HTTP = "http://"
                const val HTTPS = "https://"
                const val SERVER_PLACEHOLDER = "{server}"
                const val LOGIN_EXTENSION = "/avayatest/auth"
                const val LOGOUT_EXTENSION = "/avayatest/auth/is/"
                const val SESSION_ID_PLACEHOLDER = "{session_id}"
                const val PORT_PLACEHOLDER = "{port}"

                const val REGULAR_LOGIN_URL = HTTP + SERVER_PLACEHOLDER + ":" + PORT_PLACEHOLDER + LOGIN_EXTENSION
                const val SECURE_LOGIN_URL = HTTPS + SERVER_PLACEHOLDER + ":" + PORT_PLACEHOLDER + LOGIN_EXTENSION
                const val REGULAR_LOGOUT_URL = HTTP + SERVER_PLACEHOLDER + ":" + PORT_PLACEHOLDER + LOGOUT_EXTENSION + SESSION_ID_PLACEHOLDER
                const val SECURE_LOGOUT_URL = HTTPS + SERVER_PLACEHOLDER + ":" + PORT_PLACEHOLDER + LOGOUT_EXTENSION + SESSION_ID_PLACEHOLDER

                const val DATA_SESSION_KEY = "_session_key"
                const val DATA_KEY_SESION_ID = "_session_id"
                const val DATA_KEY_ERROR = "_error"
                const val DATA_KEY_SERVER = "_server"
                const val DATA_KEY_SECURE = "_secure"
                const val DATA_KEY_PORT = "_port"
                const val DATA_KEY_MEDIA_TYPE = "_mediaType"
                const val DATA_KEY_RESPONSE_CODE = "_responseCode"
                const val DATA_KEY_RESPONSE_MESSAGE = "_responseMessage"
                const val DATA_KEY_EXCEPTION_MESSAGE = "_exceptionMessage"

                const val KEY_ENABLE_VIDEO = "key_enableVideo"
                const val KEY_START_MUTED_AUDIO = "key_startMutedAudio"
                const val KEY_START_MUTED_VIDEO = "key_startMutedVideo"
                const val KEY_NUMBER_TO_DIAL = "key_numberToDial"
                const val KEY_CONTEXT = "key_context"
                const val KEY_PREFERRED_VIDEO_RESOLUTION = "key_preferredVideoResolution"

                const val PLACEHOLDER1 = "%1"

                const val TIMER_INTERVAL = 100L
                const val CALL_TIME_ELAPSED_FORMAT = "%s - %s%02d:%02d%s"
                const val CALL_TIME_ELAPSED_SEPARATOR = "("
                const val CALL_TIME_ELAPSED_END = ")"

                const val RESOLUTION_176x144 = "176x144"
                const val RESOLUTION_320x180 = "320x180"
                const val RESOLUTION_352x288 = "352x288"
                const val RESOLUTION_640x360 = "640x360"
                const val RESOLUTION_640x480 = "640x480"
                const val RESOLUTION_960x720 = "960x720"
                const val RESOLUTION_1280x720 = "1280x720"

                const val REPORT_ISSUE_SUBJECT = "Reporting issue " + PLACEHOLDER1

                const val PREFERENCE_EMAIL_ADDRESS = "preference_email_address"
                const val PREFERENCE_LOG_TO_DEVICE = "preference_log_to_device"
                const val PREFERENCE_LOG_LEVEL = "preference_log_level"
                const val PREFERENCE_LOG_FILE_NAME = "preference_log_file_name"
                const val PREFERENCE_MAX_FILE_SIZE = "preference_max_file_size"
                const val PREFERENCE_MAX_BACK_UPS = "preference_max_back_ups"
                const val PREFERENCE_SECURE_LOGIN = "preference_secure_login"
                const val PREFERENCE_PORT = "preference_port"
                const val PREFERENCE_TRUST_ALL_CERTS = "preference_trust_all_certs"

                const val LOG_LEVEL_DEBUG = "debug"
                const val LOG_LEVEL_INFO = "info"
                const val LOG_LEVEL_WARN = "warn"
                const val LOG_LEVEL_ERROR = "error"
                
                const val RESULT_LOGOUT = 3

                const val MINIMUM_CALL_QUALITY = 0
                const val MAXIMUM_CALL_QUALITY = 100

                const val ALPHA_NUMERIC_REGEX = "^[a-zA-Z0-9\\s]*$"

        }

}
