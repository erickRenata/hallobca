package com.bca.hallobca.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.bca.hallobca.R
import com.bca.hallobca.data.model.home.MainIcon
import kotlinx.android.synthetic.main.item_home_icon.view.*

class CommonGridAdapter: BaseAdapter {
    var menuList = ArrayList<MainIcon>()
    var context: Context? = null
    var itemLayoutId = 0

    constructor(context: Context, menuList: ArrayList<MainIcon>, itemLayoutId: Int) {
        this.context = context
        this.menuList = menuList
        this.itemLayoutId = itemLayoutId
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val menuItem = menuList[position]

        var inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var iconView = inflator.inflate(itemLayoutId, null)
        iconView.icon_image.setImageResource(menuItem.icon!!)
        iconView.icon_text.setText(menuItem.text!!)

        return iconView

    }

    override fun getItem(position: Int): Any {
        return menuList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return menuList.size
    }
}