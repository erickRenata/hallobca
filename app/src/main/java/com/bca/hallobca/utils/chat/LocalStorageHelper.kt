package com.bca.hallobca.utils.chat

import android.content.Context
import android.content.SharedPreferences
import com.gdplabs.buzz.Application.BuzzConstant
import com.gdplabs.buzz.Utils.Storages.IStorageHelper

class LocalStorageHelper(context: Context) : IStorageHelper {

    private var sharedPreferences: SharedPreferences = context.getSharedPreferences(BuzzConstant.STORAGE_NAME_KEY, 0)
    private lateinit var editor: SharedPreferences.Editor

    override fun loadDataStorage(p0: String?): String {
        return this.sharedPreferences.getString(p0, "null")!!
    }

    override fun clearDataStorage() {
        this.sharedPreferences.edit().clear().apply()
    }

    override fun saveDataStorage(p0: String?, p1: String?) {
        this.sharedPreferences.edit().putString(p0, p1).apply()
    }
}