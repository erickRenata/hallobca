package com.bca.hallobca.utils

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.bca.hallobca.R
import okhttp3.*
import java.security.MessageDigest

class Utils {

    companion object {

        fun valid(number: String): Boolean {
            var checksum: Int = 0

            for (i in number.length - 1 downTo 0 step 2) {
                checksum += number.get(i) - '0'
            }
            for (i in number.length - 2 downTo 0 step 2) {
                val n: Int = (number.get(i) - '0') * 2
                checksum += if (n > 9) n - 9 else n
            }

            return checksum%10 == 0
        }
        fun validateAccountNumber(accountNumber: String): Boolean {
            if (accountNumber.length!=10) {
                return false
            }
            var sum = 0
            var digit = 0
            var addEnd = 0
            var checkDigit = 0
            val ldig: Int
            val anArray = intArrayOf(4, 3, 2, 8, 6, 5, 4, 3, 2)
            val endDigit = Integer.parseInt(accountNumber.substring(accountNumber.length - 1, accountNumber.length))
            for (i in accountNumber.length - 1 downTo 1) {
                digit = Integer.parseInt(accountNumber.substring(i - 1, i))
                addEnd = digit * anArray[i - 1]
                sum += addEnd
            }
            checkDigit = sum - 11 * (sum / 11)
            checkDigit = 11 - checkDigit
            ldig = checkDigit % 10 //get last digit number
            return ldig == endDigit
        }

        fun validateATMNumber(cardNumber: String): Boolean {
            if (cardNumber.length!=16) {
                return false
            }
            var sum = 0
            var digit = 0
            var addEnd = 0
            var checkDigit = 0
            var doubled = true
            val endDigit = Integer.parseInt(cardNumber.substring(cardNumber.length - 1, cardNumber.length))
            for (i in cardNumber.length - 1 downTo 1) {
                digit = Integer.parseInt(cardNumber.substring(i - 1, i))
                if (doubled) {
                    addEnd = digit * 2
                    if (addEnd > 9) {
                        addEnd -= 9
                    }
                } else {
                    addEnd = digit
                }
                sum += addEnd
                doubled = !doubled
            }
            checkDigit = sum % 10
            return if (checkDigit != 0) {
                checkDigit = 10 - checkDigit
                checkDigit == endDigit
            } else {
                false
            }
        }

        fun validateCCNumber(cardNumber: String): Boolean {
            if (cardNumber.length!=16) {
                return false
            }
            var sum = 0
            var digit = 0
            var addend = 0
            var doubled = false
            println("Masuk validateCCNumber")
            for (i in cardNumber.length - 1 downTo 0) {
                digit = Integer.parseInt(cardNumber.substring(i, i + 1))
                if (doubled) {
                    addend = digit * 2
                    if (addend > 9) {
                        addend -= 9
                    }
                } else {
                    addend = digit
                }
                sum += addend
                doubled = !doubled
            }
            //System.out.println("sum = " + sum);
            return sum % 10 == 0
        }


        fun validateAccUsingApi(crType: Int, crValue: String, callback: Callback) : Call? {
            val client = OkHttpClient()
            if (crType in 1..3) {
                val request = Request.Builder().url(
                    TextUtils.concat(
                        Constant.BASE_URL, Constant.API_CHECK_ACC,
                        "?searchBy=", Constant.SEARCH_BY_LIST[crType],
                        "&cardNumber=", crValue
                    ).toString()
                ).build()
                val call = client.newCall(request)
                call.enqueue(callback)
                return call
            }
            return null
        }

        fun getPhoneNumberList(crType: Int, crValue: String, callback: Callback) : Call? {
            val client = OkHttpClient()
            if (crType in 1..3) {
                val request = Request.Builder().url(
                    TextUtils.concat(
                        Constant.BASE_URL, Constant.API_GET_PHONE,
                        "?searchBy=", Constant.SEARCH_BY_LIST[crType],
                        "&cardNumber=", crValue
                    ).toString()
                ).build()
                val call = client.newCall(request)
                call.enqueue(callback)
                return call
            }
            return null
        }

        fun getNameFromApi(crType: Int, crValue: String, callback: Callback) : Call? {
            val client = OkHttpClient()
            if (crType in 1..3) {
                val request = Request.Builder().url(
                    TextUtils.concat(
                        Constant.BASE_URL, Constant.API_GET_NAME,
                        "?searchBy=", Constant.SEARCH_BY_LIST[crType],
                        "&cardNumber=", crValue
                    ).toString()
                ).build()
                val call = client.newCall(request)
                call.enqueue(callback)
                return call
            }
            return null
        }

        fun getEmailFromApi(crType: Int, crValue: String, callback: Callback) : Call? {
            val client = OkHttpClient()
            if (crType in 1..3) {
                val request = Request.Builder().url(
                    TextUtils.concat(
                        Constant.BASE_URL, Constant.API_GET_EMAIL,
                        "?searchBy=", Constant.SEARCH_BY_LIST[crType],
                        "&cardNumber=", crValue
                    ).toString()
                ).build()
                val call = client.newCall(request)
                call.enqueue(callback)
                return call
            }
            return null
        }

        fun requestOTPFromApi(phoneNumber: String, callback: Callback) : Call? {
            val client = OkHttpClient()
            val request = Request.Builder().url(
                TextUtils.concat(
                    Constant.BASE_URL, Constant.API_GEN_OTP,
//                    "?searchBy=", Constant.SEARCH_BY_LIST[crType],
                    "?phoneNumber=", phoneNumber
                ).toString()
            ).build()
            val call = client.newCall(request)
            call.enqueue(callback)
            return call
        }

        fun showErrorSnack(view: View, text: String){
            //Snackbar(view)
            val snackbar = Snackbar.make(view, text, Snackbar.LENGTH_LONG).setAction("Action", null)
            snackbar.setActionTextColor(Color.BLUE)
            val snackbarView = snackbar.view
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                snackbarView.setBackgroundColor(view.context.getColor(R.color.errorSnack))
            } else {
                snackbarView.setBackgroundColor(ContextCompat.getColor(view.context, R.color.errorSnack))
            }
            val textView = snackbarView.findViewById(R.id.snackbar_text) as TextView
            with (textView) {
                setTextColor(Color.WHITE)
                setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL)
                setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_x, 0)
                textAlignment = View.TEXT_ALIGNMENT_CENTER
            }
            if (view is CoordinatorLayout) {
                val lp = snackbarView.layoutParams as CoordinatorLayout.LayoutParams
                lp.apply {
                    //                setMargins(leftMargin,topMargin,rightMargin,56)
                    anchorId = R.id.navigation
                    anchorGravity = Gravity.TOP
                    gravity = Gravity.TOP
                }
                snackbarView.layoutParams = lp
            }
            snackbar.show()
        }

        fun editTextEyeTouch(v: EditText, event: MotionEvent): Boolean {
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= (v.right - (v.compoundDrawablesRelative[2].bounds.width()) * 2)) {
                    var passwordVisible = false
                    if (v.getTag(R.string.tag_password_visible) != null) {
                        passwordVisible = v.getTag(R.string.tag_password_visible) as Boolean
                    }
                    if (passwordVisible) {
                        v.setTag(R.string.tag_password_visible, false)
                        v.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    } else {
                        v.setTag(R.string.tag_password_visible, true)
                        v.transformationMethod = PasswordTransformationMethod.getInstance()
                    }
                }
            }
            return false
        }

        fun hashPin(pin: String) : String {
            val bytes = pin.toByteArray()
            val md = MessageDigest.getInstance("SHA-256")
            val digest = md.digest(bytes)
            return digest.fold("", { str, it -> str + "%02x".format(it) })
        }

        fun saveCredentials(context: Context, crType: Int, crValue: String, ePin: String, phoneNumber: String) : Boolean {
            val prefs = context.getSharedPreferences(Constant.SP_GLOBAL, 0)
            val editor = prefs.edit()
            with (editor) {
                putInt(Constant.CR_TYPE, crType)
                putString(Constant.CR_VAL, crValue)
                putString(Constant.EPIN, ePin)
                putString(Constant.PHONE_NUMBER, phoneNumber)
                putBoolean(Constant.ROLE_STATUS, true)
                apply()
            }
            return true
        }

        fun saveEmail(context: Context, email: String) {
            val prefs = context.getSharedPreferences(Constant.SP_GLOBAL, 0)
            val editor = prefs.edit()
            with (editor) {
                putString(Constant.EMAIL, email)
            }.apply()
        }

        fun saveUsername(context: Context, username: String) {
            val prefs = context.getSharedPreferences(Constant.SP_GLOBAL, 0)
            val editor = prefs.edit()
            with (editor) {
                putString(Constant.UNAME, username)
            }.apply()
        }

        fun savePicPath(context: Context, absPath: String) {
            val prefs = context.getSharedPreferences(Constant.SP_GLOBAL, 0)
            val editor = prefs.edit()
            with (editor) {
                putString(Constant.ABSPP, absPath)
            }.apply()
        }

        fun getRoleStatus(context: Context) : Boolean {
            return context.getSharedPreferences(Constant.SP_GLOBAL, 0)!!.getBoolean(Constant.ROLE_STATUS, false)
        }

        fun getEncryptedPIN(context: Context) : String {
            return context.getSharedPreferences(Constant.SP_GLOBAL, 0)!!.getString(Constant.EPIN, "")!!
        }

        fun getCrType(context: Context) : Int {
            return context.getSharedPreferences(Constant.SP_GLOBAL, 0)!!.getInt(Constant.CR_TYPE, 0)
        }

        fun getCrValue(context: Context) : String {
            return context.getSharedPreferences(Constant.SP_GLOBAL, 0)!!.getString(Constant.CR_VAL, "")!!
        }

        fun getPhoneNumber(context: Context) : String {
            return context.getSharedPreferences(Constant.SP_GLOBAL, 0)!!.getString(Constant.PHONE_NUMBER, "")!!
        }

        fun getEmail(context: Context) : String {
            return context.getSharedPreferences(Constant.SP_GLOBAL, 0)!!.getString(Constant.EMAIL, "Unspecified")!!
        }

        fun getUsername(context: Context) : String{
            return context.getSharedPreferences(Constant.SP_GLOBAL, 0)!!.getString(Constant.UNAME, "Anonymous")!!
        }

        fun getPicPath(context: Context) : String {
            return context.getSharedPreferences(Constant.SP_GLOBAL, 0)!!.getString(Constant.ABSPP, "")!!
        }

        fun isAgree(context: Context) : Boolean {
            return context.getSharedPreferences(Constant.SP_GLOBAL, 0)!!.getBoolean(Constant.DISCLAIMER_AGREE, false)!!
        }

        fun setAgree(context: Context) {
            val prefs = context.getSharedPreferences(Constant.SP_GLOBAL, 0)
            val editor = prefs.edit()
            with (editor) {
                putBoolean(Constant.DISCLAIMER_AGREE, true)
            }.apply()
        }

    }
}