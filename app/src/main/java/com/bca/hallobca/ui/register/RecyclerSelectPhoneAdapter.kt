package com.bca.hallobca.ui.register

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.bca.hallobca.R

class RecyclerSelectPhoneAdapter(private val phoneList: Array<String>, val clickListener: (String, Int) -> Unit) :
    RecyclerView.Adapter<RecyclerSelectPhoneAdapter.ItemHolder>() {
    private var selectedItemPosition: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val textView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_select_phone, parent, false) as TextView
        return ItemHolder(textView)
    }

    override fun getItemCount() = phoneList.size

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.textView.text = TextUtils.concat(
            phoneList[position].substring(0, 4),
            "*******",
            phoneList[position].substring(phoneList[position].length - 3)
        )
        holder.bind(phoneList[position], clickListener, position)
        holder.textView.isSelected = false
        if (position == selectedItemPosition) {
            holder.textView.isSelected = true
        }
    }

    class ItemHolder(textView: TextView) : RecyclerView.ViewHolder(textView) {
        val textView = textView

        fun bind(text: String, clickListener: (String, Int) -> Unit, position: Int) {
            textView.setOnClickListener{clickListener(text, position)}
        }

    }

    fun setSelectedItemPosition(position: Int) {
        this.selectedItemPosition = position
        notifyDataSetChanged()
    }
}