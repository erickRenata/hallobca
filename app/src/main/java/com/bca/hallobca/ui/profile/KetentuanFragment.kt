package com.bca.hallobca.ui.profile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bca.hallobca.R

class KetentuanFragment: Fragment() {
    private lateinit var recyclerViewKetentuan: RecyclerView
    private lateinit var ketentuanViewAdapter: RecyclerView.Adapter<*>
    private lateinit var ketentuanViewManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_histmsg, container, false)
        val titleText = view.findViewById<TextView>(R.id.fragment_title)
        titleText.text = activity!!.getString(R.string.label_ketentuan)
        val backButton = view.findViewById<TextView>(R.id.back_button)
        backButton.setOnClickListener {
            activity!!.onBackPressed()
        }
        val arrayKetentuan = arrayOf(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        )

        ketentuanViewAdapter = RecyclerKetentuanProfileAdapter(arrayKetentuan)
        ketentuanViewManager = LinearLayoutManager(activity!!)
        recyclerViewKetentuan = view.findViewById<RecyclerView>(R.id.list_container).apply {
            layoutManager = ketentuanViewManager
            adapter = ketentuanViewAdapter
        }
        return view
    }

}