package com.bca.hallobca.ui.home

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bca.hallobca.R
import com.bca.hallobca.data.model.home.MainIcon
import com.bca.hallobca.ui.call.CallFragment
import com.bca.hallobca.ui.chat.ChatFragment
import com.bca.hallobca.ui.register.RegisterActivity
import com.bca.hallobca.ui.register.ToSActivity
import com.bca.hallobca.utils.CommonGridAdapter
import com.bca.hallobca.utils.Utils
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.ArrayList

class HomeFragment: Fragment() {
    private lateinit var homeMenuAdapter: CommonGridAdapter
    private var menuList = ArrayList<MainIcon>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)
        initHomeMenu(view)

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        homeMenuAdapter = CommonGridAdapter(context, menuList, R.layout.item_home_icon)

    }

    private fun initHomeMenu(view: View) {
        menuList.clear()
        menuList.add(MainIcon(R.string.home_icon_call, R.drawable.ic_main_menu_call))
        menuList.add(MainIcon(R.string.home_icon_chat, R.drawable.ic_main_menu_chat))
        menuList.add(MainIcon(R.string.home_icon_mail, R.drawable.ic_main_menu_mail))
        menuList.add(MainIcon(R.string.home_icon_twit, R.drawable.ic_main_menu_twitter))
        val grid = view.findViewById<GridView>(R.id.icon_container)
        grid.adapter = homeMenuAdapter
        grid.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            changeFragment(position)
        }
        // guest or registered
        val registerButton = view.findViewById<Button>(R.id.register_button)
        val registerInfo = view.findViewById<TextView>(R.id.register_info)
        if (!(activity!! as HomeActivity).isGuest) {
            registerButton.visibility = View.GONE
            registerInfo.visibility = View.GONE
        } else {

            registerButton.setOnClickListener {
                startActivity(Intent(activity!!, RegisterActivity::class.java))
            }
        }

    }

    private fun changeFragment(position: Int) {
        var newFragment: Fragment? = null
        when (position) {
            0 -> newFragment = CallFragment()
            1 -> newFragment = ChatFragment()
            2 -> startActivity(Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:halobca@bca.co.id")))
            3 -> shareTwitter("@HaloBCA")
        }
        if (newFragment != null) {
            val fragment: Fragment = newFragment
            this.fragmentManager!!.beginTransaction()
                .replace(R.id.content, fragment)
                .addToBackStack(null)
                .commit()
        }
    }

    private fun shareTwitter(message: String) {
        val tweetIntent = Intent(Intent.ACTION_SEND)
        tweetIntent.putExtra(Intent.EXTRA_TEXT, message)
        tweetIntent.type = "text/plain"

        val packManager = activity!!.packageManager
        val resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY)

        var resolved = false
        for (resolveInfo in resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                tweetIntent.setClassName(
                    resolveInfo.activityInfo.packageName,
                    resolveInfo.activityInfo.name
                )
                resolved = true
                break
            }
        }
        if (resolved) {
            startActivity(tweetIntent)
        } else {
            val i = Intent()
            i.putExtra(Intent.EXTRA_TEXT, message)
            i.action = Intent.ACTION_VIEW
            i.data = Uri.parse("https://twitter.com/intent/tweet?text=" + urlEncode(message))
            startActivity(i)
            Toast.makeText(activity, "Twitter app isn't found", Toast.LENGTH_LONG).show()
        }
    }

    private fun urlEncode(s: String): String {
        try {
            return URLEncoder.encode(s, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            Log.wtf("Error Twitter", "UTF-8 should always be supported", e)
        }
        return ""

    }


}