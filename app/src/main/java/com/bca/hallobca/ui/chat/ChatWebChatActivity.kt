package com.bca.hallobca.ui.chat

import android.content.Context
import android.content.Intent
import android.os.*
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.transition.Slide
import android.transition.TransitionManager
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import com.bca.hallobca.R
import com.bca.hallobca.base.BaseActivity
import com.bca.hallobca.data.model.chat.ChatItem
import com.bca.hallobca.data.model.home.MainIcon
import com.bca.hallobca.ui.call.ReviewActivity
import com.bca.hallobca.utils.Constant
import com.bca.hallobca.utils.ext.dimBehind
import kotlinx.android.synthetic.main.activity_chat_webchat.*

class ChatWebChatActivity: BaseActivity() {

    private var activePopupWindow = PopupWindow()
    private lateinit var recyclerViewChat: RecyclerView
    private lateinit var chatViewAdapter: RecyclerView.Adapter<*>
    private lateinit var chatViewManager: RecyclerView.LayoutManager
    private var dummyChat = arrayOf(
        "Hello",
        "Ola",
        "Anyeong",
        "Priviet",
        "Namaste",
        "Como Estas"
    )
    var ongoingChat = ArrayList<ChatItem>()
    companion object {
        const val AGENT_ONLINE = 0
        const val AGENT_OFFLINE = 1
        const val AGENT_AWAY = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_webchat)

        input_chat.imeOptions = EditorInfo.IME_ACTION_SEND
        input_chat.setRawInputType(InputType.TYPE_CLASS_TEXT)
        input_chat.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                sendMessage(input_chat.text.toString())
                input_chat.text.clear()
//                dismissInput()
                return@OnKeyListener true
            }
            false
        })

        close_button.setOnClickListener {
            onBackPressed()
        }
        val stringChatInitDummy = "Halo Pak\nAda yang bisa saya bantu ?"
        ongoingChat.add(ChatItem(stringChatInitDummy,false))
        chatViewAdapter = RecyclerWebChatAdapter(ongoingChat)
        chatViewManager = LinearLayoutManager(this)

        recyclerViewChat = findViewById<RecyclerView>(R.id.list_container).apply {
            layoutManager = chatViewManager
            adapter = chatViewAdapter

        }

        dummyAgentActivity()
    }

    private fun showConfirmationPopup(parent: View) {
        val inflater: LayoutInflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_profile_guest, null)

        activePopupWindow = PopupWindow(view,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        val confirmationTextView = view.findViewById<TextView>(R.id.popup_message)
        confirmationTextView.text = getString(R.string.chat_end_confirmation)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activePopupWindow.elevation = 10.0F
            activePopupWindow.setBackgroundDrawable(this.getDrawable(R.drawable.popup_background))
            activePopupWindow.isOutsideTouchable = true
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            activePopupWindow.enterTransition = slideIn

            val slideOut = Slide()
            slideOut.slideEdge = Gravity.BOTTOM
            activePopupWindow.exitTransition = slideOut

        }

        val confirmButton = view.findViewById<Button>(R.id.confirm_button)
        val cancelButton = view.findViewById<Button>(R.id.cancel_button)

        confirmButton.setOnClickListener {
            activePopupWindow.dismiss()
            val currentChat = MainIcon(R.string.chat_feature_name, R.drawable.ic_icon_web_chat_orange)
            startActivity(
                Intent(this, ReviewActivity::class.java)
                    .putExtra(Constant.CALL_DEST, currentChat)
            )
        }
        cancelButton.setOnClickListener {
            activePopupWindow.dismiss()
        }

        activePopupWindow.setOnDismissListener {
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(parent as ViewGroup)
        }
        activePopupWindow.showAtLocation(parent, Gravity.CENTER, 0, 0)
        activePopupWindow.dimBehind()

    }

    override fun onBackPressed() {
        showConfirmationPopup(rootLayout)
    }

    private fun sendMessage(message: String) {
        ongoingChat.add(ChatItem(message, true))
        recyclerViewChat.scrollToPosition(ongoingChat.size-1)
    }

    private fun receiveMessage(message: String) {
        ongoingChat.add(ChatItem(message, false))
        recyclerViewChat.scrollToPosition(ongoingChat.size-1)
    }

    private fun changeAgentOnlineStatus(agentStatus: Int) {
        when (agentStatus) {
            AGENT_ONLINE -> {
                icon_online_status.setImageResource(R.drawable.dot_grey_border)
                text_online_status.text = getString(R.string.chat_status_online)
            }
            AGENT_AWAY -> {
                icon_online_status.setImageResource(R.drawable.dot_grey_border_away)
                text_online_status.text = getString(R.string.chat_status_away)
            }
            AGENT_OFFLINE -> {
                icon_online_status.setImageResource(R.drawable.dot_grey_border_offline)
                text_online_status.text = getString(R.string.chat_status_offline)
            }
            else -> {
                icon_online_status.setImageResource(R.drawable.dot_grey_border_offline)
                text_online_status.text = getString(R.string.chat_status_offline)
            }
        }
    }

    private fun dismissInput() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(input_chat.windowToken, 0)
    }

    private fun dummyAgentActivity() {

        Handler(Looper.getMainLooper()).post {
            object : CountDownTimer(300000, 8000) {

                override fun onFinish() {
                    receiveMessage("Terima Kasih")
                }

                override fun onTick(millisUntilFinished: Long) {
                    receiveMessage(dummyChat.asIterable().shuffled().take(1)[0])
                    changeAgentOnlineStatus((0..2).asIterable().shuffled().take(1)[0])
                }

            }.start()
        }
    }

}