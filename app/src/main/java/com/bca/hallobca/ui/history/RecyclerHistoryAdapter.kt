package com.bca.hallobca.ui.history

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bca.hallobca.R
import com.bca.hallobca.data.model.home.HistoryItem
import kotlinx.android.synthetic.main.item_history.view.*

class RecyclerHistoryAdapter(private val historyArray: ArrayList<HistoryItem>) :
    RecyclerView.Adapter<RecyclerHistoryAdapter.ItemHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_history, parent, false) as CardView
        return RecyclerHistoryAdapter.ItemHolder(cardView)
    }

    override fun getItemCount() = historyArray.size

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        with (holder.cardView) {
            history_icon.setImageResource(historyArray[position].historyIcon!!)
            call_title.text = historyArray[position].historyTitle
            id_laporan.text = historyArray[position].historyId
            call_date.text = historyArray[position].historyDate
            call_time.text = historyArray[position].historyTime
        }
    }

    class ItemHolder(cardView: CardView) : RecyclerView.ViewHolder(cardView) {
        val cardView = cardView
    }
}