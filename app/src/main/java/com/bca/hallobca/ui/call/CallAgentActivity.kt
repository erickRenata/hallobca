package com.bca.hallobca.ui.call

import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.avaya.clientplatform.api.*
import com.bca.hallobca.R
import com.bca.hallobca.data.model.call.ClientPlatformManager
import com.bca.hallobca.data.model.home.MainIcon
import com.bca.hallobca.utils.Constant
import kotlinx.android.synthetic.main.activity_agent_call_screen.*

class CallAgentActivity : AppCompatActivity(), AudioOnlySessionListener, AudioOnlyUserListener {

    private lateinit var mTimeHandler : Handler
    private lateinit var mediaPlayer : MediaPlayer

    private var mDevice: AudioOnlyDevice? = null
    private var mUser: AudioOnlyUser? = null
    private var mPlatform: AudioOnlyClientPlatform? = null
    private var mSession: AudioOnlySession? = null

    private var mCallOnHold = false
    private var mStartAudioMuted = false
    private var mContextId: String? = null
    private val mCallTimeChecker = object: Runnable {
        override fun run() {
            updateCallTime()
            mTimeHandler.postDelayed(this, Constant.TIMER_INTERVAL)
        }
    }
    override fun onConnReestablished(p0: AudioOnlyUser?) {
    }

    override fun onServiceAvailable(p0: AudioOnlyUser?) {
        if (mSession == null) {
            if (mUser!!.isServiceAvailable) {
                call()
            } else {
                hangup()
            }
        } else {
            //
        }
    }

    override fun onConnRetry(p0: AudioOnlyUser?) {
    }

    override fun onConnectionInProgress(p0: AudioOnlyUser?) {
    }

    override fun onConnLost(p0: AudioOnlyUser?) {
    }

    override fun onServiceUnavailable(p0: AudioOnlyUser?) {
        // log here
    }

    override fun onNetworkError(p0: AudioOnlyUser?) {
    }

    override fun onCriticalError(p0: AudioOnlyUser?) {
    }

    override fun onSessionRemoteAlerting(p0: AudioOnlySession?, p1: Boolean) {
        mediaPlayer.start()
        mediaPlayer.isLooping = true
    }

    override fun onSessionRemoteAddressChanged(p0: AudioOnlySession?, p1: String?, p2: String?) {
    }

    override fun onSessionEnded(p0: AudioOnlySession?) {
        hangup()
        finish()
    }

    override fun onSessionFailed(p0: AudioOnlySession?, p1: SessionError?) {
    }

    override fun onSessionQueued(p0: AudioOnlySession?) {
    }

    override fun onDialError(p0: AudioOnlySession?, p1: SessionError?, p2: String?, p3: String?) {
    }

    override fun onGetMediaError(p0: AudioOnlySession?) {
    }

    override fun onSessionRedirected(p0: AudioOnlySession?) {
    }

    override fun onSessionServiceAvailable(p0: AudioOnlySession?) {
    }

    override fun onSessionServiceUnavailable(p0: AudioOnlySession?) {
    }

    override fun onQualityChanged(p0: AudioOnlySession?, p1: Int) {
    }

    override fun onSessionEstablished(p0: AudioOnlySession?) {
        try {
            //val sessionState = mSession!!.state
            mediaPlayer.stop()

        }catch (e: Exception) {

        }
    }

    override fun onSessionRemoteDisplayNameChanged(p0: AudioOnlySession?, p1: String?) {
        // nothing to change
    }

    override fun onSessionAudioMuteFailed(p0: AudioOnlySession?, p1: Boolean, p2: SessionException?) {
    }

    override fun onCallError(p0: AudioOnlySession?, p1: SessionError?, p2: String?, p3: String?) {
    }

    override fun onCapacityReached(p0: AudioOnlySession?) {
    }

    override fun onSessionAudioMuteStatusChanged(p0: AudioOnlySession?, p1: Boolean) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agent_call_screen)
        val currentCall = intent.getSerializableExtra(Constant.CALL_DEST) as? MainIcon

        call_icon.setImageResource(currentCall!!.icon!!)
        call_information.text = getString(R.string.call_screen_agent_text, getString(currentCall.text!!))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.statusBarColor = getColor(R.color.colorBlack)
        }
        btn_end_call.setOnClickListener {
            endcall()
            startActivity(Intent(this, ReviewActivity::class.java)
                .putExtra(Constant.CALL_DEST, currentCall)
            )
        }
        mTimeHandler = Handler()
        mediaPlayer = MediaPlayer.create(this, R.raw.ringback)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPlatform = null
        mUser = null
    }

    override fun onResume() {
        super.onResume()
        try {
            val extras = intent.extras
            mContextId = extras.getString(Constant.KEY_CONTEXT).trim()
            mStartAudioMuted = extras.getBoolean(Constant.KEY_START_MUTED_AUDIO)

            mPlatform = ClientPlatformManager.getAudioOnlyClientPlatform(this)
            mUser = mPlatform!!.user

            val token = extras.getString(Constant.DATA_SESSION_KEY)

            val tokenAccepted = mUser!!.setSessionAuthorizationToken(token)

            if (tokenAccepted) {
                mUser!!.registerListener(this)
                mUser!!.acceptAnyCertificate(true)

                mDevice = mPlatform!!.device

                if (mDevice != null) {
                    //some log to check device using mDevice.couldMediaBeAccessible(getApplicationContext)
                }

                if (mSession == null) {
                    if (mUser!!.isServiceAvailable) {
                        // make call here
                        call()
                    }
                } else {
                    //log cannot make call session in use
                }
            } else {
                // invalid token cannot make call
            }
        } catch (e: Exception) {
            // silent exception
            // log why error here
        }
    }

    fun updateCallTime() {

    }

    fun call() {
        try {
            mDevice = mPlatform!!.device
            val clientPlatform = ClientPlatformManager.getAudioOnlyClientPlatform(this)
            val browser = clientPlatform.userAgentBrowser
            val version = clientPlatform.userAgentVersion

            mSession = mUser!!.createSession()
            mSession!!.registerListener(this)
            mSession!!.muteAudio(mStartAudioMuted)
            if (mContextId!=null && mContextId!!.length>0) {
                mSession!!.contextId = mContextId
            }

            val numberToDial = intent.extras.getString(Constant.KEY_NUMBER_TO_DIAL)
            mSession!!.remoteAddress = numberToDial

            mSession!!.start()


        } catch (e: Exception) {
            finish()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        hangup()
        finish()
        mediaPlayer.stop()
    }

    private fun endcall() {
        try {
            hangup()
            finish()
            mediaPlayer.stop()
        } catch (e: Exception) {

        }
    }

    fun toggleMuteAudio() {
        try {
            val wasMuted = mSession!!.isAudioMuted
            val nowMuted = !wasMuted

            mSession!!.muteAudio(nowMuted)

        } catch (e: Exception) {

        }
    }

    fun toggleHold() {
        if (!mCallOnHold) {
            mSession!!.hold()
            mCallOnHold = true
        } else {
            mSession!!.resume()
            mCallOnHold = false
        }
    }

    private fun hangup() {
        try {
            if (mSession!=null) {
                mSession!!.unregisterListener(this)
                mUser!!.unregisterListener(this)
                mSession!!.end()
                mediaPlayer.stop()
            }
        } catch (e: Exception) {

        }
    }
}
