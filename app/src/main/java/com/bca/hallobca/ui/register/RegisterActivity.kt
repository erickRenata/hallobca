package com.bca.hallobca.ui.register

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.transition.Slide
import android.transition.TransitionManager
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.bca.hallobca.R
import com.bca.hallobca.base.BaseActivity
import com.bca.hallobca.ui.home.HomeActivity
import com.bca.hallobca.ui.otp.OtpActivity
import com.bca.hallobca.utils.Constant
import com.bca.hallobca.utils.Utils
import com.bca.hallobca.utils.ext.dimBehind
import kotlinx.android.synthetic.main.activity_register.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException

class RegisterActivity : BaseActivity(), AdapterView.OnItemSelectedListener{

    var inputUserArr = arrayOf(
        Constant.SEL_CR_TYPE,
        Constant.CR_TYPE_1,
        Constant.CR_TYPE_2,
        Constant.CR_TYPE_3
    )
    private lateinit var activePopupWindow: PopupWindow

    private lateinit var recyclerViewPhoneList: RecyclerView
    private lateinit var phoneListViewAdapter: RecyclerView.Adapter<*>
    private lateinit var phoneListViewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initSpinner()

        btn_masuk.setOnClickListener {
            screenHideError()
            if (validateInput()) {
//                progress_bar.visibility = View.VISIBLE
//                checkInputNumber()
                //bypass online test
//                afterGetPhoneList("081908712123")
                afterGetPhoneList("081908712123, 081908712124, 081908712125")
            }
        }
        btn_masuk.isEnabled = false

        et_pin.setOnTouchListener { v, event -> Utils.editTextEyeTouch(v as EditText, event)}
        et_pin_confirmation.setOnTouchListener { v, event ->  Utils.editTextEyeTouch(v as EditText, event)}

        // dummy autofil card no for testing
//        et_rekening.setText("6019002501167573")
//        et_pin.setText("999999")
//        et_pin_confirmation.setText("999999")

        et_rekening.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                etRekeningHideError()
                screenHideError()
                enableButtonWhenAllSets()
            }
        })
        et_pin.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                etPinHideError()
                screenHideError()
                enableButtonWhenAllSets()
            }
        })
        et_pin_confirmation.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                etPinConfirmationHideError()
                screenHideError()
                enableButtonWhenAllSets()
            }
        })
        back_button.setOnClickListener {
            onBackPressed()
        }

    }

    private fun initSpinner() {
        spinner_register!!.onItemSelectedListener = this
        spinner_register!!.isFocusable = true
        spinner_register!!.isFocusableInTouchMode = true
        val aa = ArrayAdapter(this, R.layout.spinner_register_item, inputUserArr)
        aa.setDropDownViewResource(R.layout.spinner_register_selected)
        spinner_register!!.adapter = aa
        spinner_register.setSelection(0)
        spinner_register.requestFocus()
        spinner_register.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                spinner_register.performClick()
            }
        }

    }

//    private fun initPopupMultiPhone(phoneList: Array<String>) {
//        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//        val view = inflater.inflate(R.layout.popup_select_phone, null)
//        activePopupWindow = PopupWindow(view,
//            LinearLayout.LayoutParams.WRAP_CONTENT,
//            LinearLayout.LayoutParams.WRAP_CONTENT
//        )
//
//        phoneListViewManager = LinearLayoutManager(this)
//        phoneListViewAdapter = RecyclerSelectPhoneAdapter(phoneList) { text: String -> selectPhoneItemClicked(text)}
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            activePopupWindow.elevation = 10.0F
//            activePopupWindow.setBackgroundDrawable(getDrawable(R.drawable.popup_background))
//            activePopupWindow.isOutsideTouchable = true
//        }
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            val slideIn = Slide()
//            slideIn.slideEdge = Gravity.TOP
//            activePopupWindow.enterTransition = slideIn
//
//            val slideOut = Slide()
//            slideOut.slideEdge = Gravity.BOTTOM
//            activePopupWindow.exitTransition = slideOut
//
//        }
//
//        recyclerViewPhoneList = view.findViewById<RecyclerView>(R.id.select_phone_content).apply {
//            layoutManager = phoneListViewManager
//            adapter = phoneListViewAdapter
//
//        }
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            TransitionManager.beginDelayedTransition(rootLayout)
//        }
//        activePopupWindow.showAtLocation(rootLayout, Gravity.CENTER, 0, 0)
//        activePopupWindow.dimBehind()
//
//    }

    private fun initPopupUnregisteredPhone() {
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_unregistered_phone, null)
        activePopupWindow = PopupWindow(view,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activePopupWindow.elevation = 10.0F
            activePopupWindow.setBackgroundDrawable(getDrawable(R.drawable.popup_background))
            activePopupWindow.isOutsideTouchable = true
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            activePopupWindow.enterTransition = slideIn

            val slideOut = Slide()
            slideOut.slideEdge = Gravity.BOTTOM
            activePopupWindow.exitTransition = slideOut

        }

        val closePopup = view.findViewById<TextView>(R.id.close_popup_button)
        closePopup.setOnClickListener{
            activePopupWindow.dismiss()
        }
        closePopup.visibility = View.GONE

        activePopupWindow.setOnDismissListener {
            Handler(Looper.getMainLooper()).postDelayed({
                finish()
                this@RegisterActivity.startActivity(Intent(this@RegisterActivity, HomeActivity::class.java)
                    .putExtra(Constant.ROLE_GUEST, true)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            }, 100)
        }

        val popupMessage = view.findViewById<TextView>(R.id.popup_message)

//        val ss = SpannableString(popupMessage.text)
//
//        ss.setSpan(CustomClickableSpan(this, activePopupWindow), ss.length - 5, ss.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
//
//        popupMessage.text = ss
        popupMessage.isClickable = true
        popupMessage.movementMethod = LinkMovementMethod.getInstance()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(rootLayout)
        }
        activePopupWindow.showAtLocation(rootLayout, Gravity.CENTER, 0, 0)
        activePopupWindow.dimBehind()
    }

    private fun validateInput(): Boolean {
        var validationResult = true
        if (et_rekening.text.isNotEmpty()) {
            if (et_rekening.text.toString().equals("9999")) {
                return true
            }
        }
        if (et_pin_confirmation.text.toString() != et_pin.text.toString()) {
            et_pin_confirmation_msg.text = getString(R.string.error_message_empty_missmatch_pin)
            et_pin_confirmation_msg.visibility = View.VISIBLE
            validationResult = false
        }
        if (spinner_register.selectedItemPosition == 1) {
            if (et_rekening.text.length != 10) {
                et_rekening_msg.text = getString(R.string.error_message_wrong_account_number)
                et_rekening_msg.visibility = View.VISIBLE
                validationResult = false
            }
            if (!Utils.valid(et_rekening.text.toString())) {
                et_rekening_msg.text = getString(R.string.error_message_wrong_account_number)
                et_rekening_msg.visibility = View.VISIBLE
                validationResult = false
            }
        } else {
            if (et_rekening.text.length != 16) {
                et_rekening_msg.text = getString(R.string.error_message_wrong_card_number)
                et_rekening_msg.visibility = View.VISIBLE
                validationResult = false
            }
            if (!Utils.valid(et_rekening.text.toString())) {
                et_rekening_msg.text = getString(R.string.error_message_wrong_card_number)
                et_rekening_msg.visibility = View.VISIBLE
                validationResult = false
            }
        }
        if (spinner_register.selectedItemPosition == 0) {
            spinner_register_msg.text = getString(R.string.error_message_unselected_card_type)
            spinner_register_msg.visibility = View.VISIBLE
            validationResult = false
        }
        if (et_rekening.text.isEmpty()) {
            if (spinner_register.selectedItemPosition == 1) {
                et_rekening_msg.text = getString(R.string.error_message_empty_account_number)
                et_rekening_msg.visibility = View.VISIBLE
            } else {
                et_rekening_msg.text = getString(R.string.error_message_empty_card_number)
                et_rekening_msg.visibility = View.VISIBLE
            }
            validationResult = false
        }
        if (et_pin.text.length != 6) {
            et_pin_msg.text = getString(R.string.error_message_error_limit_pin)
            et_pin_msg.visibility = View.VISIBLE
            validationResult = false
        }
        if (et_pin.text.isEmpty()) {
            et_pin_msg.text = getString(R.string.error_message_empty_pin)
            et_pin_msg.visibility = View.VISIBLE
            validationResult = false
        }
        if (et_pin_confirmation.text.length != 6) {
            et_pin_confirmation_msg.text = getString(R.string.error_message_error_limit_pin_confirmation)
            et_pin_confirmation_msg.visibility = View.VISIBLE
            validationResult = false
        }
        if (et_pin_confirmation.text.isEmpty()) {
            et_pin_confirmation_msg.text = getString(R.string.error_message_empty_pin_confirmation)
            et_pin_confirmation_msg.visibility = View.VISIBLE
            validationResult = false
        }
        return validationResult
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        if (position == 1) {
            et_rekening!!.hint = getString(R.string.register_account_number_hint);
        } else {
            et_rekening!!.hint = getString(R.string.register_card_number_hint);
        }
        spinner_register_msg.visibility = View.GONE
        etRekeningHideError()
        screenHideError()
        enableButtonWhenAllSets()
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {

    }

    private fun selectPhoneItemClicked(text: String) {
        if (activePopupWindow.isShowing) {
            activePopupWindow.dismiss()
        }
        val bundle = Bundle()
        bundle.putInt(Constant.CR_TYPE, spinner_register.selectedItemPosition)
        bundle.putString(Constant.CR_VAL, et_rekening.text.toString())
        bundle.putString(Constant.EPIN, Utils.hashPin(et_pin.text.toString()))
        bundle.putString(Constant.PHONE_NUMBER, text)
        startActivity(Intent(this, OtpActivity::class.java)
            .putExtras(bundle)
        )
    }

    override fun onBackPressed() {
        if (::activePopupWindow.isInitialized && activePopupWindow.isShowing) {
            Handler(Looper.getMainLooper()).postDelayed({
                super.onBackPressed()
            }, 100)
            activePopupWindow.dismiss()
        } else {
            super.onBackPressed()
        }
    }

    class CustomClickableSpan(context: Context, popupContainer: PopupWindow) : ClickableSpan() {
        val context = context
        val popupContainer = popupContainer
        override fun onClick(widget: View) {
            popupContainer.dismiss()
            context.startActivity(Intent(context, HomeActivity::class.java)
                .putExtra(Constant.ROLE_GUEST, true)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            (context as Activity).finish()
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }

    }

    fun afterGetPhoneList(string: String) {
        val phoneList = string.split(",").toTypedArray()
        if (phoneList.size == 1) {
            if (phoneList[0].isEmpty() || phoneList[0].contains("fa")) {
                initPopupUnregisteredPhone()
            } else {
                val bundle = Bundle()
                bundle.putInt(Constant.CR_TYPE, spinner_register.selectedItemPosition)
                bundle.putString(Constant.CR_VAL, et_rekening.text.toString())
                bundle.putString(Constant.PHONE_NUMBER, string)
                bundle.putString(Constant.EPIN, Utils.hashPin(et_pin.text.toString()))
                startActivity(
                    Intent(this, PhoneSelectActivity::class.java)
                        .putExtras(bundle)
                )
            }
        } else {
//            initPopupMultiPhone(phoneList)
            val bundle = Bundle()
            bundle.putInt(Constant.CR_TYPE, spinner_register.selectedItemPosition)
            bundle.putString(Constant.CR_VAL, et_rekening.text.toString())
            bundle.putString(Constant.PHONE_NUMBER, string)
            bundle.putString(Constant.EPIN, Utils.hashPin(et_pin.text.toString()))
            startActivity(
                Intent(this, PhoneSelectActivity::class.java)
                    .putExtras(bundle)
            )
        }
    }

    fun requestPhoneNumberList() {
        val getPhoneList = Utils.getPhoneNumberList(
            spinner_register.selectedItemPosition,
            et_rekening.text.toString(),
            object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    runOnUiThread {
                        progress_bar.visibility = View.GONE
                        this@RegisterActivity.screenShowError(this@RegisterActivity.getString(R.string.bad_connection_message))
                    }
                }

                override fun onResponse(call: Call, response: Response) {
                    val responseBody = response.body()?.string()
                    Log.i("CEK", responseBody)
                    runOnUiThread {
                        progress_bar.visibility = View.GONE
                        afterGetPhoneList(responseBody ?: "")
                    }
                }

            }
        )
        if (getPhoneList == null) {
            this@RegisterActivity.spinner_register_msg.text = this@RegisterActivity.getString(R.string.error_message_unselected_card_type)
            this@RegisterActivity.spinner_register_msg.visibility = View.VISIBLE
        }
    }

    private fun checkInputNumber() {
        // check crval online
        val checkCrVal = Utils.validateAccUsingApi(spinner_register.selectedItemPosition, et_rekening.text.toString(),
            object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    runOnUiThread {
                        progress_bar.visibility = View.GONE
                        this@RegisterActivity.screenShowError(this@RegisterActivity.getString(R.string.bad_connection_message))
                    }
                }

                override fun onResponse(call: Call, response: Response) {
                    val responseData = response.body()?.string()
                    runOnUiThread {
                        progress_bar.visibility = View.GONE
                        this@RegisterActivity.requestPhoneNumberList()
//                        if ((responseData ?: "false").toBoolean()) {
//                            this@RegisterActivity.requestPhoneNumberList()
//                        } else {
//                            runOnUiThread {
//                                progress_bar.visibility = View.GONE
//                                if (spinner_register.selectedItemPosition == 1) {
//                                    this@RegisterActivity.et_rekening_msg.text = this@RegisterActivity.getString(R.string.error_message_wrong_account_number)
//                                    this@RegisterActivity.et_rekening_msg.visibility = View.VISIBLE
//                                } else {
//                                    this@RegisterActivity.et_rekening_msg.text = this@RegisterActivity.getString(R.string.error_message_wrong_card_number)
//                                    this@RegisterActivity.et_rekening_msg.visibility = View.VISIBLE
//                                }
//                            }
//                        }
                    }
                }

            }
        )
        if (checkCrVal == null) {
            this@RegisterActivity.spinner_register_msg.text = this@RegisterActivity.getString(R.string.error_message_unselected_card_type)
            this@RegisterActivity.spinner_register_msg.visibility = View.VISIBLE
        }
    }

    private fun etRekeningHideError() {
        et_rekening_msg.text = ""
        et_rekening_msg.visibility = View.GONE
    }

    private fun etPinHideError() {
        et_pin_msg.text = ""
        et_pin_msg.visibility = View.GONE
    }

    private fun etPinConfirmationHideError() {
        et_pin_confirmation_msg.text = ""
        et_pin_confirmation_msg.visibility = View.GONE
    }

    private fun screenHideError() {
        screen_msg.text = ""
        screen_msg.visibility = View.GONE
    }

    fun screenShowError(message: String) {
        screen_msg.text = message
        screen_msg.visibility = View.VISIBLE
    }

    fun enableButtonWhenAllSets() {
        btn_masuk.isEnabled = false
        if (
            spinner_register.selectedItemPosition>0
            && !et_rekening.text.isNullOrEmpty()
            && !et_pin.text.isNullOrEmpty()
            && !et_pin_confirmation.text.isNullOrEmpty()
        ) {
            btn_masuk.isEnabled = true
        }
    }


}

