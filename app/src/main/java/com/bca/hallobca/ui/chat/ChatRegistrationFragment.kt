package com.bca.hallobca.ui.chat

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.v4.app.Fragment
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.bca.hallobca.R
import com.bca.hallobca.utils.Utils
import com.gdplabs.buzz.Application.BuzzConstant
import com.gdplabs.buzz.Application.Main.ChatActivity
import kotlinx.android.synthetic.main.fragment_webchat_registration.*

class ChatRegistrationFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_webchat_registration, container, false)
        val backButton = view.findViewById<TextView>(R.id.back_button)
        val startButton = view.findViewById<Button>(R.id.btn_start_chat)
        startButton.setOnClickListener {
            if (isValidInput()) {
                startChat()
            }
        }
        backButton.setOnClickListener {
            activity!!.onBackPressed()
        }
        return view
    }

    private fun isValidInput(): Boolean {
        val snackbarContainer = activity!!.findViewById<CoordinatorLayout>(R.id.snackbar_container)
        if (et_name.text.isEmpty()) {
            Utils.showErrorSnack(snackbarContainer, activity!!.getString(R.string.error_empty_name))
            return false
        }
        if (et_email.text.isEmpty()) {
            Utils.showErrorSnack(snackbarContainer, activity!!.getString(R.string.error_empty_email))
            return false
        }
        if (et_nomor_hp.text.isEmpty()) {
            Utils.showErrorSnack(snackbarContainer, activity!!.getString(R.string.error_empty_hp))
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(et_email.text).matches()) {
            Utils.showErrorSnack(snackbarContainer, activity!!.getString(R.string.error_format_email))
            return false
        }
        if (et_nomor_hp.length() < 9) {
            Utils.showErrorSnack(snackbarContainer, activity!!.getString(R.string.error_short_hp))
            return false
        }
        return true
    }

    private fun startChat() {
//        startActivity(Intent(context, ChatWaitingActivity::class.java))
        val intent = Intent(context, ChatActivity::class.java)
        intent.putExtra(BuzzConstant.BUNDLE_CUSTOMER_NAME_KEY, et_name.text)
        intent.putExtra(BuzzConstant.BUNDLE_CUSTOMER_EMAIL_KEY, et_email.text)
        intent.putExtra(BuzzConstant.BUNDLE_CUSTOMER_PHONE_NUMBER_KEY, et_nomor_hp.text)
        intent.putExtra(BuzzConstant.BUNDLE_CUSTOMER_ACCESSED_FROM_KEY, "BCA Mobile")
        startActivity(intent)
    }

}