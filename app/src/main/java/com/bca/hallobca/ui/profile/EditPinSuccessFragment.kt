package com.bca.hallobca.ui.profile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bca.hallobca.R

class EditPinSuccessFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_edit_email_success, container, false)
        val successText = view.findViewById<TextView>(R.id.change_email_info)
        successText.text = activity!!.getString(R.string.change_pin_success)
        val fragmentTitle = view.findViewById<TextView>(R.id.fragment_title)
        fragmentTitle.text = activity!!.getString(R.string.profile_edit_pin_title)
        val backButton = view.findViewById<TextView>(R.id.back_button)
        backButton.setOnClickListener {
            activity!!.onBackPressed()
        }
        return view
    }
}