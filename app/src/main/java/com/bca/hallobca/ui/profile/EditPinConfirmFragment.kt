package com.bca.hallobca.ui.profile

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import com.bca.hallobca.R
import com.bca.hallobca.ui.home.HomeActivity
import com.bca.hallobca.utils.Constant
import com.bca.hallobca.utils.Utils
import kotlinx.android.synthetic.main.activity_home.*

class EditPinConfirmFragment: Fragment() {

    private lateinit var screenError: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_edit_old_pin, container, false)
        initInputPinDisplay(view)
        val backButton = view.findViewById<TextView>(R.id.back_button)
        backButton.setOnClickListener {
            hideScreenError()
            activity!!.onBackPressed()
        }

        screenError = view.findViewById(R.id.screen_error)
        return view
    }

    private fun initInputPinDisplay(view: View) {
        val infoPin = view.findViewById<TextView>(R.id.input_old_pin_info)
        infoPin.text = activity!!.getString(R.string.input_reentry_pin)
        val inputPin = view.findViewById<EditText>(R.id.input_data)
        val d1 = view.findViewById<TextView>(R.id.digit_pin1)
        val d2 = view.findViewById<TextView>(R.id.digit_pin2)
        val d3 = view.findViewById<TextView>(R.id.digit_pin3)
        val d4 = view.findViewById<TextView>(R.id.digit_pin4)
        val d5 = view.findViewById<TextView>(R.id.digit_pin5)
        val d6 = view.findViewById<TextView>(R.id.digit_pin6)
        d1.setOnClickListener {
            dotDisplayClick(view)
        }
        d2.setOnClickListener {
            dotDisplayClick(view)
        }
        d3.setOnClickListener {
            dotDisplayClick(view)
        }
        d4.setOnClickListener {
            dotDisplayClick(view)
        }
        d5.setOnClickListener {
            dotDisplayClick(view)
        }
        d6.setOnClickListener {
            dotDisplayClick(view)
        }
        inputPin.addTextChangedListener ( object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                hideScreenError()
                val dotEmpty = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    activity!!.getDrawable(R.drawable.dot_pin_empty)
                } else {
                    ContextCompat.getDrawable(activity!!, R.drawable.dot_pin_empty)
                }
                val dotFilled = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    activity!!.getDrawable(R.drawable.dot_pin_filled)
                } else {
                    ContextCompat.getDrawable(activity!!, R.drawable.dot_pin_filled)
                }
                if (s != null) {
                    d1.setCompoundDrawablesRelativeWithIntrinsicBounds(null, dotEmpty, null, null)
                    d2.setCompoundDrawablesRelativeWithIntrinsicBounds(null, dotEmpty, null, null)
                    d3.setCompoundDrawablesRelativeWithIntrinsicBounds(null, dotEmpty, null, null)
                    d4.setCompoundDrawablesRelativeWithIntrinsicBounds(null, dotEmpty, null, null)
                    d5.setCompoundDrawablesRelativeWithIntrinsicBounds(null, dotEmpty, null, null)
                    d6.setCompoundDrawablesRelativeWithIntrinsicBounds(null, dotEmpty, null, null)
                    if (s.isNotEmpty()) {
                        d1.setCompoundDrawablesRelativeWithIntrinsicBounds(null, dotFilled, null, null)
                    }
                    if (s.length>1) {
                        d2.setCompoundDrawablesRelativeWithIntrinsicBounds(null, dotFilled, null, null)
                    }
                    if (s.length>2) {
                        d3.setCompoundDrawablesRelativeWithIntrinsicBounds(null, dotFilled, null, null)
                    }
                    if (s.length>3) {
                        d4.setCompoundDrawablesRelativeWithIntrinsicBounds(null, dotFilled, null, null)
                    }
                    if (s.length>4) {
                        d5.setCompoundDrawablesRelativeWithIntrinsicBounds(null, dotFilled, null, null)
                    }
                    if (s.length>5) {
                        d6.setCompoundDrawablesRelativeWithIntrinsicBounds(null, dotFilled, null, null)
                        if (this@EditPinConfirmFragment.pinValidated()) {
                            for (i in 1..activity!!.supportFragmentManager.backStackEntryCount) {
                                activity!!.supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                            }
                            val epin = Utils.hashPin(s.toString())
                            val sharedPreferences = activity!!.getSharedPreferences(Constant.SP_GLOBAL, 0)
                            sharedPreferences.edit().putString(Constant.EPIN, epin).apply()
                            (activity!! as HomeActivity).setNewPin("")
                            val fragment: Fragment = EditPinSuccessFragment()
                            this@EditPinConfirmFragment.fragmentManager!!.beginTransaction()
                                .replace(R.id.content, fragment)
                                .addToBackStack(null)
                                .commit()
                        } else {
                            activity!!.onBackPressed()
                        }
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        } )
    }

    private fun dotDisplayClick(view: View) {
        val inputPin = view.findViewById<EditText>(R.id.input_data)
        inputPin.requestFocus()
        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(inputPin, InputMethodManager.SHOW_IMPLICIT)
    }

    private fun pinValidated() : Boolean{
        val inputPin = view!!.findViewById<EditText>(R.id.input_data)
        val epin = inputPin.text.toString()
        val sepin = (activity!! as HomeActivity).getNewPin()
        if (sepin != epin) {
//            Utils.showErrorSnack(activity!!.snackbar_container, activity!!.getString(R.string.error_old_pin))
            showScreenError(activity!!.getString(R.string.error_old_pin))
            return false
        }

        return true
    }
    fun hideScreenError() {
        screenError.visibility = View.GONE
    }

    fun showScreenError(text: String) {
        screenError.text = text
        screenError.visibility = View.VISIBLE
    }
}
