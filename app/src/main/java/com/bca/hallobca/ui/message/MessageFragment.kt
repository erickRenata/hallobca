package com.bca.hallobca.ui.message

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bca.hallobca.R
import com.bca.hallobca.data.model.home.MessageItem

class MessageFragment: Fragment() {
    private lateinit var recyclerMessageAdapter: RecyclerView
    private lateinit var messageViewManager: RecyclerView.LayoutManager
    private lateinit var messageViewAdapter: RecyclerView.Adapter<*>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_histmsg, container, false)
        val titleText = view.findViewById<TextView>(R.id.fragment_title)
        titleText.text = activity!!.getString(R.string.message_fragment_title)
        val backButton = view.findViewById<TextView>(R.id.back_button)
        backButton.setOnClickListener {
            activity!!.onBackPressed()
        }

        val arrayMessage = ArrayList<MessageItem>()

        messageViewAdapter = RecyclerMessageAdapter(arrayMessage)
        messageViewManager = LinearLayoutManager(activity!!)

        recyclerMessageAdapter = view.findViewById<RecyclerView>(R.id.list_container).apply {
            adapter = messageViewAdapter
            layoutManager = messageViewManager
        }
        return view
    }

}