package com.bca.hallobca.ui.chat

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.bca.hallobca.R
import com.bca.hallobca.base.BaseActivity
import kotlinx.android.synthetic.main.activity_chat_waiting.*

class ChatWaitingActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_waiting)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            rootLayout.systemUiVisibility = rootLayout.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.statusBarColor= getColor(R.color.colorWhite)
        }
        Handler().postDelayed({
            startActivity(Intent(this, ChatWebChatActivity::class.java))
        }, 1800)
        cancel_button.setOnClickListener {
            onBackPressed()
        }
    }
}