package com.bca.hallobca.ui.otp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.*
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.transition.Slide
import android.transition.TransitionManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Toast
import com.bca.hallobca.R
import com.bca.hallobca.base.BaseActivity
import com.bca.hallobca.ui.home.HomeActivity
import com.bca.hallobca.ui.opening.OpeningScreen
import com.bca.hallobca.utils.Constant
import com.bca.hallobca.utils.Utils
import com.bca.hallobca.utils.ext.dimBehind
import kotlinx.android.synthetic.main.activity_otp.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException


class OtpActivity : BaseActivity() {

    var secUntilFinished: Long = 0
    private var otpEntryCount = 0
    private var resendCount = 0
    private lateinit var prefs: SharedPreferences
    private lateinit var activePopupWindow: PopupWindow
    private var nameCached = false
    private var emailCached = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)


        if (intent.extras == null) {
            startActivity(Intent(this, HomeActivity::class.java)
                .putExtra(Constant.ROLE_GUEST, true)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            finish()
        }

        prefs = this.getSharedPreferences(Constant.SP_GLOBAL, 0)

        val bundle = intent.extras!!
        val phoneNumber = bundle.getString(Constant.PHONE_NUMBER)

        if (phoneNumber.isNullOrEmpty()) {
            onBackPressed()
        }
        initOTPDisplay(phoneNumber!!)
        initOTPResend()

        btn_masuk.setOnClickListener {
            hideScreenError()
            if (!validateOTP()) {
                otpEntryCount++
                if (otpEntryCount > 3) {
                    initPopupOTPMaxTries()
                } else {
//                    Utils.showErrorSnack(this.findViewById(R.id.content), getString(R.string.otp_validation_failed, 4 - otpEntryCount))
                    showScreenError(getString(R.string.otp_validation_failed, 4 - otpEntryCount))
                }
            } else {
                Utils.saveCredentials(
                    this,
                    bundle.getInt(Constant.CR_TYPE, 0),
                    bundle.getString(Constant.CR_VAL, ""),
                    bundle.getString(Constant.EPIN, ""),
                    bundle.getString(Constant.PHONE_NUMBER, "")
                )

                if (!(nameCached || emailCached)) {
                    if (!nameCached) Utils.saveUsername(this, "Ahmad W. Setiarto")
                    if (!emailCached) Utils.saveEmail(this, "ahmad.setiarto@gmail.com")
                }
                otpFinished()
            }
        }

        progress_bar.visibility = View.VISIBLE
        Handler().postDelayed(
            {
                runOnUiThread {
                    // request
//                    requestOTP(phoneNumber)
                    progress_bar.visibility = View.GONE
                    requestName()
                    requestEmail()
                    startTimmer()
                }
            },
            2000
        )

//        startSmsRetriever()
    }

//    private fun startSmsRetriever() {
//        val client = SmsRetriever.getClient(this)
//
//        val task = client.startSmsRetriever()
//
//        task.addOnSuccessListener { _ -> Log.d("OtpActivity", "Sms listener started!") }
//        task.addOnFailureListener { e ->
//            Log.e("OtpActivity", "Failed to start sms retriever: ${e.message}")
//        }
//    }

    private fun startTimmer() {
        Handler(Looper.getMainLooper()).post {
            object : CountDownTimer(300000, 1000) {

                override fun onTick(millisUntilFinished: Long) {
                    secUntilFinished = millisUntilFinished / 1000
                    val mnt = "" + secUntilFinished.div(60)
                    val sec = "" + secUntilFinished.rem(60)
                    otp_timeout_display.text = getString(
                        R.string.timer_display,
                        mnt.padStart(2,'0'),
                        sec.padStart(2, '0')
                    )
                }

                override fun onFinish() {
                    otp_timeout_display.text = getString(R.string.timer_off)
                    secUntilFinished = 0
                }
            }.start()
        }
    }

    private fun initOTPDisplay(phoneNumber: String) {
        val maskedPhoneNumber = TextUtils.concat(
            phoneNumber.substring(0, 4),
            "*******",
            phoneNumber.substring(phoneNumber.length - 3)
        )
        tv_header.text = getString(R.string.otp_screen_main_text, maskedPhoneNumber)
        digit_otp1.setOnClickListener {
            otpDisplayClick()
        }
        digit_otp2.setOnClickListener {
            otpDisplayClick()
        }
        digit_otp3.setOnClickListener {
            otpDisplayClick()
        }
        digit_otp4.setOnClickListener {
            otpDisplayClick()
        }
        digit_otp5.setOnClickListener {
            otpDisplayClick()
        }
        digit_otp6.setOnClickListener {
            otpDisplayClick()
        }

        btn_masuk.isEnabled = false

        input_otp.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    digit_otp1.text = ""
                    digit_otp2.text = ""
                    digit_otp3.text = ""
                    digit_otp4.text = ""
                    digit_otp5.text = ""
                    digit_otp6.text = ""
                    if (s.isNotEmpty()) {
                        digit_otp1.text = s[0].toString()
                    }
                    if (s.length > 1) {
                        digit_otp2.text = s[1].toString()
                    }
                    if (s.length > 2) {
                        digit_otp3.text = s[2].toString()
                    }
                    if (s.length > 3) {
                        digit_otp4.text = s[3].toString()
                    }
                    if (s.length > 4) {
                        digit_otp5.text = s[4].toString()
                    }
                    if (s.length > 5) {
                        digit_otp6.text = s[5].toString()
                        btn_masuk.isEnabled = true
                    } else {
                        btn_masuk.isEnabled = false
                    }
                    if (s.length > 6) {
                        Toast.makeText(this@OtpActivity, "perform validation", Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })

    }

    private fun otpDisplayClick() {
        input_otp.isEnabled = true
        input_otp.isFocusableInTouchMode = true
        input_otp.isFocusable = true
        input_otp.requestFocus()
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(input_otp, InputMethodManager.SHOW_IMPLICIT)
        hideScreenError()
    }

    private fun initOTPResend() {
        otp_resend_link.isClickable = true
        otp_resend_link.movementMethod = LinkMovementMethod.getInstance()
        otp_resend_link.setOnClickListener {
            hideScreenError()
            if (secUntilFinished > 0) {
//                Utils.showErrorSnack(this.findViewById(R.id.content), getString(R.string.otp_resend_before_timeout))
                showScreenError(getString(R.string.otp_resend_before_timeout))
            } else {
                if (resendCount>3) {
                    showScreenError(getString(R.string.otp_resend_max_tries))
                } else {
                    //resend here

                    startTimmer()
                    resendCount++
                    updateOtpRequestInfo(resendCount)
                }
            }
        }
        resendCount++
        updateOtpRequestInfo(resendCount)
    }

    private fun initPopupOTPMaxTries() {
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_otp_max_tries, null)

        activePopupWindow = PopupWindow(view,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activePopupWindow.elevation = 10.0F
            activePopupWindow.setBackgroundDrawable(getDrawable(R.drawable.popup_background))
            activePopupWindow.isOutsideTouchable = true
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            activePopupWindow.enterTransition = slideIn

            val slideOut = Slide()
            slideOut.slideEdge = Gravity.BOTTOM
            activePopupWindow.exitTransition = slideOut

        }

        activePopupWindow.setOnDismissListener {
            Handler(Looper.getMainLooper()).postDelayed({
                finish()
            }, 100)
        }

        val regButton = view.findViewById<Button>(R.id.register_button)
        regButton.setOnClickListener {
            activePopupWindow.dismiss()
            startActivity(
                Intent(this, OpeningScreen::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            )
            finish()
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(content)
        }
        activePopupWindow.showAtLocation(content, Gravity.CENTER, 0, 0)
        activePopupWindow.dimBehind()

    }

    private fun validateOTP() : Boolean {
        val otp = input_otp.text.toString()
        if (otp.length < 6) {
            return false
        }
        if (otp.startsWith("444444")) {
            return true
        }
        return false
    }

    private fun requestName() {
        val bundle = intent.extras!!
        val getName = Utils.getNameFromApi(
            bundle.getInt(Constant.CR_TYPE, 0),
            bundle.getString(Constant.CR_VAL, ""),
            object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    runOnUiThread {
                        // silent fails
//                        progress_bar.visibility = View.GONE
//                        Utils.showErrorSnack(this@OtpActivity.findViewById(android.R.id.content),
//                            this@OtpActivity.getString(R.string.bad_connection_message)
//                        )
                    }
                }

                override fun onResponse(call: Call, response: Response) {
                    val responseBody = response.body()?.string()
                    runOnUiThread {
//                        progress_bar.visibility = View.GONE
                        if (!responseBody.isNullOrEmpty()) {
                            Utils.saveUsername(this@OtpActivity, responseBody)
                            nameCached = true
//                            Utils.showErrorSnack(this@OtpActivity.findViewById(android.R.id.content),
//                                responseBody
//                            )
                        }
//                        else {
//                            Utils.showErrorSnack(this@OtpActivity.findViewById(android.R.id.content),
//                                "Empty response"
//                            )
//                        }
                    }
                }

            }
        )
        if (getName == null) {
            // silent fails
//            Utils.showErrorSnack(this@OtpActivity.findViewById(android.R.id.content),
//                getString(R.string.error_message_unselected_card_type)
//            )
        }
    }


    private fun requestEmail() {
        val bundle = intent.extras!!
        val getEmail = Utils.getEmailFromApi(
            bundle.getInt(Constant.CR_TYPE, 0),
            bundle.getString(Constant.CR_VAL, ""),
            object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    runOnUiThread {
                        // silent fails
//                        progress_bar.visibility = View.GONE
//                        Utils.showErrorSnack(this@OtpActivity.findViewById(android.R.id.content),
//                            this@OtpActivity.getString(R.string.bad_connection_message)
//                        )
                    }
                }

                override fun onResponse(call: Call, response: Response) {
                    val responseBody = response.body()?.string()
                    runOnUiThread {
//                        progress_bar.visibility = View.GONE
                        if (!responseBody.isNullOrEmpty()) {
                            Utils.saveEmail(this@OtpActivity, responseBody)
                            emailCached = true
//                            Utils.showErrorSnack(this@OtpActivity.findViewById(android.R.id.content),
//                                responseBody
//                            )
                        }
//                        else {
//                            Utils.showErrorSnack(this@OtpActivity.findViewById(android.R.id.content),
//                                "Empty response"
//                            )
//                        }
                    }
                }

            }
        )
        if (getEmail == null) {
            // fails..
//            Utils.showErrorSnack(this@OtpActivity.findViewById(android.R.id.content),
//                getString(R.string.error_message_unselected_card_type)
//            )
        }
    }

    private fun requestOTP(phoneNumber: String) {
        val call = Utils.requestOTPFromApi(phoneNumber,
            object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    runOnUiThread {
                        // silent fails
                        progress_bar.visibility = View.GONE
//                        Utils.showErrorSnack(this@OtpActivity.findViewById(android.R.id.content),
//                            this@OtpActivity.getString(R.string.bad_connection_message)
//                        )
                    }
                }

                override fun onResponse(call: Call, response: Response) {
                    val responseBody = response.body()?.string()
                    runOnUiThread {
                        progress_bar.visibility = View.GONE
                        if (!responseBody.isNullOrEmpty()) {
                            startTimmer()
                        }
                    }
                }

            }
        )
    }

    private fun otpFinished() {
        startActivity(Intent(this, HomeActivity::class.java)
            .putExtra(Constant.ROLE_GUEST, false)
            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
        finish()
    }

    private fun hideScreenError() {
        error_text.text = ""
        error_text.visibility = View.GONE
    }

    private fun showScreenError(text: String) {
        error_text.text = text
        error_text.visibility = View.VISIBLE
    }

    private fun updateOtpRequestInfo(int: Int) {
        info_otp_attempt.text = getString(R.string.otp_attempt_text, int)
    }


}
