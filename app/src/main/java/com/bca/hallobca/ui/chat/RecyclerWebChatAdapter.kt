package com.bca.hallobca.ui.chat

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.bca.hallobca.R
import com.bca.hallobca.data.model.chat.ChatItem

class RecyclerWebChatAdapter(private val chatItemList: ArrayList<ChatItem>) :
        RecyclerView.Adapter<RecyclerWebChatAdapter.ItemHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        var view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_chat_incoming, parent, false) as ConstraintLayout
        when (viewType) {
            0 -> view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_chat_outgoing, parent, false) as ConstraintLayout
        }
        return ItemHolder(view)
    }

    override fun getItemCount() = chatItemList.size

    override fun getItemViewType(position: Int): Int {
        return if (chatItemList[position].asSend!!) 0 else 1
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.chat_text).text = chatItemList[position].message
    }

    class ItemHolder(val view: ConstraintLayout) : RecyclerView.ViewHolder(view)
}