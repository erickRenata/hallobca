package com.bca.hallobca.ui.call

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.transition.Slide
import android.transition.TransitionManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bca.hallobca.R
import com.bca.hallobca.data.model.home.MainIcon
import com.bca.hallobca.ui.home.HomeActivity
import com.bca.hallobca.utils.CommonGridAdapter
import com.bca.hallobca.utils.Constant
import com.bca.hallobca.utils.ext.dimBehind
import java.util.*

class CallFragment: Fragment() {
    private lateinit var activePopupWindow: PopupWindow
    private lateinit var callMenuAdapter: CommonGridAdapter
    private var menuList = ArrayList<MainIcon>()
    private var popIconList = ArrayList<Int>()
    private var destination = ArrayList<String>()
    private lateinit var bundle: Bundle
    private lateinit var currentCall: MainIcon

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_call, container, false)
        initCallMenu(view)
        bundle = Bundle()
        currentCall = MainIcon(0, 0)
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callMenuAdapter = CommonGridAdapter(context, menuList, R.layout.item_call_icon)
    }

    private fun initCallMenu(view: View) {
        menuList.clear()
        menuList.add(MainIcon(R.string.call_icon_perbankan, R.drawable.ic_call_perbankan))
        menuList.add(MainIcon(R.string.call_icon_kartu_kredit, R.drawable.ic_call_kartu_kredit))
        menuList.add(MainIcon(R.string.call_icon_bca_prioritas, R.drawable.ic_call_bca_prioritas))
        menuList.add(MainIcon(R.string.call_icon_kredit_konsumer, R.drawable.ic_call_kredit_consumer))
        menuList.add(MainIcon(R.string.call_icon_bca_finance, R.drawable.ic_call_bca_finance))
        menuList.add(MainIcon(R.string.call_icon_bca_multifinance, R.drawable.ic_call_bca_multifinance))
        menuList.add(MainIcon(R.string.call_icon_bca_insurance, R.drawable.ic_call_bca_insurance))
        menuList.add(MainIcon(R.string.call_icon_bca_life, R.drawable.ic_call_bca_life))
        menuList.add(MainIcon(R.string.call_icon_bca_sekuritas, R.drawable.ic_call_bca_sekuritas))
        menuList.add(MainIcon(R.string.call_icon_dummy, R.drawable.ic_call_dummy))
        menuList.add(MainIcon(R.string.call_icon_bca_syariah, R.drawable.ic_call_bca_syariah))
        menuList.add(MainIcon(R.string.call_icon_dummy, R.drawable.ic_call_dummy))
        menuList.add(MainIcon(R.string.call_icon_dummy, R.drawable.ic_call_dummy))
        popIconList.clear()
        popIconList.add(R.drawable.ic_icon_perbankan_blue)
        popIconList.add(R.drawable.ic_icon_kartu_kredit_blue)
        popIconList.add(R.drawable.ic_icon_bca_prioritas_blue)
        popIconList.add(R.drawable.ic_icon_kredit_konsumer_blue)
        popIconList.add(R.drawable.ic_icon_bca_finance_blue)
        popIconList.add(R.drawable.ic_icon_bca_multifinance_blue)
        popIconList.add(R.drawable.ic_icon_bca_insurance_blue)
        popIconList.add(R.drawable.ic_icon_bca_life_blue)
        popIconList.add(R.drawable.ic_icon_bca_sekuritas_blue)
        popIconList.add(0)
        popIconList.add(R.drawable.ic_icon_bca_syariah_blue)
        popIconList.add(0)
        popIconList.add(0)
        val grid = view.findViewById<GridView>(R.id.icon_container)
        grid.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            if (popIconList[position] != 0)
            showCallConfirmationPopup(view, position)
        }
        grid.adapter = callMenuAdapter
        val backButton = view.findViewById<TextView>(R.id.back_button)
        backButton.setOnClickListener {
            activity!!.onBackPressed()
        }

    }

    private fun showCallConfirmationPopup(parent: View, position: Int) {
        val inflater: LayoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_call_confirmation, null)

        activePopupWindow = PopupWindow(view,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activePopupWindow.elevation = 10.0F
            activePopupWindow.setBackgroundDrawable(activity!!.getDrawable(R.drawable.popup_background))
            activePopupWindow.isOutsideTouchable = true
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            activePopupWindow.enterTransition = slideIn

            val slideOut = Slide()
            slideOut.slideEdge = Gravity.BOTTOM
            activePopupWindow.exitTransition = slideOut

        }

        val popupIcon = view.findViewById<ImageView>(R.id.info_image)
        popupIcon.setImageResource(popIconList[position])

        val popupInfoText = view.findViewById<TextView>(R.id.popup_message)
        val infoText = activity!!.getString(R.string.call_confirmation_message,
            activity!!.getString(menuList[position].text!!))
        val ss = SpannableString(infoText)
        val spanColor = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity!!.getColor(R.color.spanBlue)
        } else {
            ContextCompat.getColor(activity!!, R.color.spanBlue)
        }
        currentCall.text = menuList[position].text
        currentCall.icon = popIconList[position]
        ss.setSpan(ForegroundColorSpan(spanColor), 36, infoText.length - 2, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE)
        popupInfoText.text = ss

        val confirmButton = view.findViewById<Button>(R.id.confirm_button)
        val cancelButton = view.findViewById<Button>(R.id.cancel_button)

        confirmButton.setOnClickListener {
            activePopupWindow.dismiss()
            showPopupBadNetwork(parent)
        }
        cancelButton.setOnClickListener {
            activePopupWindow.dismiss()
        }

        activePopupWindow.setOnDismissListener {
            (activity!! as HomeActivity).setPopupWindow(null)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(parent as ViewGroup)
        }
        activePopupWindow.showAtLocation(parent, Gravity.CENTER, 0, 0)
        activePopupWindow.dimBehind()
        (activity!! as HomeActivity).setPopupWindow(activePopupWindow)

    }

    private fun showPopupBadNetwork(parent: View) {
        val inflater: LayoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_bad_connection, null)

        activePopupWindow = PopupWindow(view,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activePopupWindow.elevation = 10.0F
            activePopupWindow.setBackgroundDrawable(activity!!.getDrawable(R.drawable.popup_background))
            activePopupWindow.isOutsideTouchable = true
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            activePopupWindow.enterTransition = slideIn

            val slideOut = Slide()
            slideOut.slideEdge = Gravity.BOTTOM
            activePopupWindow.exitTransition = slideOut

        }

        val retryButton = view.findViewById<Button>(R.id.retry_button)
        retryButton.setOnClickListener {
            activePopupWindow.dismiss()
            activity!!.startActivityFromFragment(this,
                Intent(context, CallAgentActivity::class.java)
                    .putExtra(Constant.CALL_DEST, currentCall)
                , 0)
        }

        activePopupWindow.setOnDismissListener {
            (activity!! as HomeActivity).setPopupWindow(null)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(parent as ViewGroup)
        }
        activePopupWindow.showAtLocation(parent, Gravity.CENTER, 0, 0)
        activePopupWindow.dimBehind()

        (activity!! as HomeActivity).setPopupWindow(activePopupWindow)

    }

}