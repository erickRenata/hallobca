package com.bca.hallobca.ui.profile

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.transition.Slide
import android.transition.TransitionManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bca.hallobca.R
import com.bca.hallobca.ui.home.HomeActivity
import com.bca.hallobca.ui.login.LoginActivity
import com.bca.hallobca.ui.register.RegisterActivity
import com.bca.hallobca.utils.Constant
import com.bca.hallobca.utils.Utils
import com.bca.hallobca.utils.ext.dimBehind
import java.io.File

class ProfileFragment: Fragment() {

    private var guestMode = false
    private var activePopupWindow = PopupWindow()
    private var inputUserArr = arrayOf(
        Constant.SEL_CR_TYPE,
        Constant.CR_TYPE_1,
        Constant.CR_TYPE_2,
        Constant.CR_TYPE_3
    )
    private lateinit var profilePicContainer: ImageView
    private lateinit var labelNomorRekening : TextView
    private lateinit var valueEmail : TextView
    private lateinit var valueNomorHandphone : TextView
    private lateinit var valueNomorRekening : TextView
    private lateinit var profileNameText : TextView
    private lateinit var fragmentView : View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        guestMode = (activity as HomeActivity).isGuest
        val view: View = inflater.inflate(R.layout.fragment_profile, container, false)
        fragmentView = view
        profileNameText = view.findViewById<TextView>(R.id.profile_name_text)
        labelNomorRekening = view.findViewById<TextView>(R.id.label_nomor_rekening)
        valueNomorRekening = view.findViewById<TextView>(R.id.value_nomor_rekening)
        valueNomorHandphone = view.findViewById<TextView>(R.id.value_nomor_handphone)
        valueEmail = view.findViewById<TextView>(R.id.value_email)
        val valuePIN = view.findViewById<TextView>(R.id.value_pin)
        val linkKetentuan = view.findViewById<TextView>(R.id.link_ketentuan)
        val valueVersi = view.findViewById<TextView>(R.id.value_versi)
        val linkEditProfile = view.findViewById<TextView>(R.id.link_edit_profile)
        val linkEditEmail = view.findViewById<TextView>(R.id.edit_email)
        val linkEditPIN = view.findViewById<TextView>(R.id.edit_pin)
        val linkHapusData = view.findViewById<TextView>(R.id.link_hapus)
        val cardHapus = view.findViewById<CardView>(R.id.container_link_hapus)
        val linkLogout = view.findViewById<TextView>(R.id.link_logout)
        val cardLogout = view.findViewById<CardView>(R.id.container_link_logout)

        val manager = activity!!.packageManager
        val info = manager.getPackageInfo(activity!!.packageName, PackageManager.GET_ACTIVITIES)
        profilePicContainer = view.findViewById(R.id.image_profile)

        valueVersi.text = info.versionName

        if (guestMode) {
            valueNomorRekening.visibility = View.GONE
            valueNomorHandphone.visibility = View.GONE
            valueEmail.visibility = View.GONE
            valuePIN.visibility = View.GONE
            linkEditProfile.visibility = View.GONE
            linkEditEmail.visibility = View.GONE
            linkEditPIN.visibility = View.GONE
            cardHapus.visibility = View.GONE
            cardLogout.visibility = View.GONE
        } else {
            linkLogout.setOnClickListener {
                showConfirmationPopup(view, 1)
            }
            linkHapusData.setOnClickListener {
                showConfirmationPopup(view, 0)
            }
            linkEditEmail.setOnClickListener {
                changeFragment(0)
            }

            linkEditPIN.setOnClickListener {
                changeFragment(1)
            }
            linkEditProfile.setOnClickListener {
                requestChangePic()
            }
        }

        linkKetentuan.setOnClickListener {
            val fragment: Fragment = KetentuanFragment()
            this.fragmentManager!!.beginTransaction()
                .replace(R.id.content, fragment)
                .addToBackStack(null)
                .commit()
        }
        cardLogout.visibility = View.GONE
        return view
    }

    override fun onResume() {
        if (!guestMode) initUserData()
        super.onResume()
    }

    fun showGuestPopup(parent: View) {
        val inflater: LayoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_profile_guest, null)

        activePopupWindow = PopupWindow(view,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activePopupWindow.elevation = 10.0F
            activePopupWindow.setBackgroundDrawable(activity!!.getDrawable(R.drawable.popup_background))
            activePopupWindow.isOutsideTouchable = true
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            activePopupWindow.enterTransition = slideIn

            val slideOut = Slide()
            slideOut.slideEdge = Gravity.BOTTOM
            activePopupWindow.exitTransition = slideOut

        }

        val confirmButton = view.findViewById<Button>(R.id.confirm_button)
        val cancelButton = view.findViewById<Button>(R.id.cancel_button)

        confirmButton.setOnClickListener {
            activePopupWindow.dismiss()
            activity!!.startActivityFromFragment(this, Intent(context, RegisterActivity::class.java), 0)
        }
        cancelButton.setOnClickListener {
            activePopupWindow.dismiss()
        }

        activePopupWindow.setOnDismissListener {
            try {
                (activity!! as HomeActivity).setPopupWindow(null)
                (activity!! as HomeActivity).goHome()
            } catch (e: Exception) {

            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(parent as ViewGroup)
        }
        activePopupWindow.showAtLocation(parent, Gravity.CENTER, 0, 0)
        activePopupWindow.dimBehind()
        (activity!! as HomeActivity).setPopupWindow(activePopupWindow)

    }

    private fun showConfirmationPopup(parent: View, flag: Int) {
        // flag 0: Delete, 1: Logout
        val inflater: LayoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_profile_guest, null)

        activePopupWindow = PopupWindow(view,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        val confirmationTextView = view.findViewById<TextView>(R.id.popup_message)
        if (flag == 0) {
            confirmationTextView.text = activity!!.getString(R.string.popup_delete_confirmation_message)
        } else {
            confirmationTextView.text = activity!!.getString(R.string.popup_logout_confirmation_message)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activePopupWindow.elevation = 10.0F
            activePopupWindow.setBackgroundDrawable(activity!!.getDrawable(R.drawable.popup_background))
            activePopupWindow.isOutsideTouchable = true
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            activePopupWindow.enterTransition = slideIn

            val slideOut = Slide()
            slideOut.slideEdge = Gravity.BOTTOM
            activePopupWindow.exitTransition = slideOut

        }

        val confirmButton = view.findViewById<Button>(R.id.confirm_button)
        val cancelButton = view.findViewById<Button>(R.id.cancel_button)

        if (flag == 0) {
            confirmButton.setOnClickListener {
                activePopupWindow.dismiss()
                val prefs = activity!!.getSharedPreferences(Constant.SP_GLOBAL, 0)
                prefs.edit().clear().commit()
                activity!!.startActivityFromFragment(this, Intent(context, HomeActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    , 0)
                activity!!.finish()
            }
        } else {
            confirmButton.setOnClickListener {
                activePopupWindow.dismiss()
                activity!!.startActivityFromFragment(this, Intent(context, LoginActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    , 0)
                activity!!.finish()
            }
        }
        cancelButton.setOnClickListener {
            activePopupWindow.dismiss()
        }

        activePopupWindow.setOnDismissListener {
            (activity!! as HomeActivity).setPopupWindow(null)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionManager.beginDelayedTransition(parent as ViewGroup)
        }
        activePopupWindow.showAtLocation(parent, Gravity.CENTER, 0, 0)
        activePopupWindow.dimBehind()
        (activity!! as HomeActivity).setPopupWindow(activePopupWindow)

    }

    private fun changeFragment(mode: Int) {
        var newFragment: Fragment? = null
        when (mode) {
            0 -> newFragment = EditEmailOldFragment()
            1 -> newFragment = EditPinOldFragment()
        }
        if (newFragment != null) {
            val fragment: Fragment = newFragment
            this.fragmentManager!!.beginTransaction()
                .replace(R.id.content, fragment)
                .addToBackStack(null)
                .commit()
        }
    }

    private fun requestChangePic() {
        (activity!! as HomeActivity).requestCameraIntent(this)
    }

    fun setProfilePicFromBitmap(hasResult: Boolean, fileAbsPath: String?) {
        if (hasResult) {
            Utils.savePicPath(activity!!, fileAbsPath!!)
            loadProfilePicture(fileAbsPath)
        }
    }

    private fun loadProfilePicture(fileAbsPath: String) {
        val picFile = File(fileAbsPath)
        if (picFile.exists()) {
            val bitmap = BitmapFactory.decodeFile(fileAbsPath)
            val thumbnail = ThumbnailUtils.extractThumbnail(bitmap, 300, 300)
            profilePicContainer.setImageBitmap(thumbnail)
        }
    }

    private fun initUserData() {
        val crType = Utils.getCrType(activity!!)
        labelNomorRekening.text = inputUserArr[crType]
        var crValue = Utils.getCrValue(activity!!)
        if (crValue.isNotEmpty()) {
            crValue = crValue.substring(0, crValue.length - 4) + "****"
        }
        valueNomorRekening.text = crValue
        var phoneNumber = Utils.getPhoneNumber(activity!!)
        if (phoneNumber.isNotEmpty()) {
            phoneNumber = phoneNumber.substring(0, phoneNumber.length - 3) + "***"
        }
        valueNomorHandphone.text = phoneNumber
        val emailAddress = Utils.getEmail(activity!!)
        valueEmail.text = if (emailAddress.contains('@'))
            emailAddress.substring(0, emailAddress.indexOf('@')-4) +
                    "****" + emailAddress.substring(emailAddress.indexOf('@'))
        else emailAddress
        val username = Utils.getUsername(activity!!)
        profileNameText.text = username
        val picPath = Utils.getPicPath(activity!!)
        if (picPath.isNotEmpty()) {
            loadProfilePicture(picPath)
        }
    }

}