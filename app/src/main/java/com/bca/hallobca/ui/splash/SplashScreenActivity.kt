package com.bca.hallobca.ui.splash

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.bca.hallobca.R
import com.bca.hallobca.ui.home.HomeActivity
import com.bca.hallobca.ui.register.ToSActivity
import com.bca.hallobca.utils.Utils
import kotlinx.android.synthetic.main.activity_splash_screen.*


class SplashScreenActivity : AppCompatActivity() {

    private val splashTime: Long = 1800 // 3 sec

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val manager = this.packageManager
        val info = manager.getPackageInfo(this.packageName, PackageManager.GET_ACTIVITIES)

        version_text!!.setText("v. " + info.versionName)

        Handler().postDelayed({
            finish()
            if (Utils.isAgree(this@SplashScreenActivity)) {
                startActivity(
                    Intent(this, HomeActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                )
            } else {
                startActivity(
                    Intent(this, ToSActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                )
            }
        }, splashTime)


    }
}
