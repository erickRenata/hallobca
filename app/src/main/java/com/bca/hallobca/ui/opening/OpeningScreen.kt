package com.bca.hallobca.ui.opening

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bca.hallobca.R
import com.bca.hallobca.ui.home.HomeActivity
import com.bca.hallobca.ui.register.RegisterActivity
import com.bca.hallobca.utils.Constant.Network.ROLE_GUEST
import kotlinx.android.synthetic.main.activity_opening_screen.*

class OpeningScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opening_screen)

        btn_daftar.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }

        tv_guest.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java)
                .putExtra(ROLE_GUEST, true)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            finish()
        }
    }
}
