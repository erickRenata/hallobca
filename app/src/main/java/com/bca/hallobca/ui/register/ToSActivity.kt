package com.bca.hallobca.ui.register

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.bca.hallobca.R
import com.bca.hallobca.base.BaseActivity
import com.bca.hallobca.ui.home.HomeActivity
import com.bca.hallobca.ui.register.RecyclerKetentuanAdapter
import com.bca.hallobca.utils.Utils
import kotlinx.android.synthetic.main.activity_tos.*

class ToSActivity: BaseActivity() {
    private lateinit var ketentuanViewAdapter: RecyclerView.Adapter<*>
    private lateinit var ketentuanViewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tos)
        val arrayKetentuan = arrayOf(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        )

        ketentuanViewAdapter = RecyclerKetentuanAdapter(arrayKetentuan)
        ketentuanViewManager = LinearLayoutManager(this)
        syarat_ketentuan_content.apply {
            adapter = ketentuanViewAdapter
            layoutManager = ketentuanViewManager
        }
        cb_disclaimer.setOnCheckedChangeListener { cb, _ ->
            btn_masuk.isEnabled = false
            if (cb.isChecked) {
                btn_masuk.isEnabled = true
            }
        }
        btn_masuk.isEnabled = false
        btn_masuk.setOnClickListener {
            Utils.setAgree(this)
            startActivity(Intent(this, HomeActivity::class.java)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
        }
    }
}