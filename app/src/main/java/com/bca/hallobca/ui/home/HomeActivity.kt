package com.bca.hallobca.ui.home

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.util.Log
import android.widget.PopupWindow
import android.widget.Toast
import com.bca.hallobca.R
import com.bca.hallobca.base.BaseActivity
import com.bca.hallobca.ui.chat.ChatFragment
import com.bca.hallobca.ui.history.HistoryFragment
import com.bca.hallobca.ui.login.LoginFragment
import com.bca.hallobca.ui.message.MessageFragment
import com.bca.hallobca.ui.profile.ProfileFragment
import com.bca.hallobca.utils.Constant
import com.bca.hallobca.utils.Utils
import com.bca.hallobca.utils.ext.disableShiftMode
import kotlinx.android.synthetic.main.activity_home.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class HomeActivity : BaseActivity() {
    private var doubleBackPressed = false
    var isGuest = false
    private var fragmentPopupWindow: PopupWindow? = null
    private var newPin = ""
    private var lastTakenPicPath: String? = null
    private var callerFragment: Fragment? = null
    var isLogon = false
    private var isShowingProfile = false
    private val idleLogoutTimeout = 360000L
    private val idleHandler = Handler(Handler.Callback {
        true
    })
    private val idleCallback = Runnable {
        goHome()
    }

    private fun resetIdleTimer() {
        Log.i("TIMER", "restart")
        idleHandler.removeCallbacks(idleCallback)
        idleHandler.postDelayed(idleCallback, idleLogoutTimeout)
    }

    private fun stopIdleTimer() {
        Log.i("TIMER", "stop")
        idleHandler.removeCallbacks(idleCallback)
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        if (isShowingProfile) {
            resetIdleTimer()
        }
    }

    override fun onResume() {
        super.onResume()
        if (isShowingProfile) {
            resetIdleTimer()
        }
    }

    override fun onStop() {
        super.onStop()
        stopIdleTimer()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        isGuest = Utils.getRoleStatus(this) == false
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.disableShiftMode()
        val fragment = HomeFragment()
        addFragment(fragment)

    }

    override fun onBackPressed() {
        if (fragmentPopupWindow != null) {
            fragmentPopupWindow!!.dismiss()
        } else {
            if (supportFragmentManager.backStackEntryCount == 0) {
                if (!doubleBackPressed) {
                    this.doubleBackPressed = true
                    Toast.makeText(this, "Tekan lagi tombol kembali untuk keluar", Toast.LENGTH_SHORT).show()
                    Handler().postDelayed({ doubleBackPressed = false }, 2000)
                    return
                }
            }
            super.onBackPressed()
        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_home -> {
                val fragment = HomeFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_history -> {
                val fragment = HistoryFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_message -> {
                val fragment = MessageFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_profile -> {
                if (isLogon || isGuest) {
//                if (isGuest) {
                    val fragment = ProfileFragment()
                    addFragment(fragment)
                } else {
                    val fragment = LoginFragment()
                    addFragment(fragment)
                }
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun addFragment(fragment: Fragment) {
        for (i in 0..supportFragmentManager.backStackEntryCount) {
            supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)
            .replace(R.id.content, fragment, fragment.javaClass.simpleName)
            .commit()
        if (fragment is ProfileFragment) {
            if (isGuest) {
                Handler().postDelayed(
                    {
                        if (fragment.view != null)
                            fragment.showGuestPopup(fragment.view!!)
                    }, 200
                )
                isShowingProfile = false
            } else {
                isShowingProfile = true
                resetIdleTimer()
            }
        } else {
            isShowingProfile = false
        }
    }

    fun setPopupWindow(activePopupWindow: PopupWindow?) {
        this.fragmentPopupWindow = activePopupWindow
    }

    fun setNewPin(pin: String) {
        this.newPin = pin
    }

    fun getNewPin() : String {
        return this.newPin
    }


    private fun checkPersmissionForCam(): Boolean {
        return (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
            android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermissionForCam() {
        val permissions = arrayOf(
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA
        )
        ActivityCompat.requestPermissions(this, permissions, Constant.PERMISSION_REQ_CODE_CAM)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            Constant.PERMISSION_REQ_CODE_CAM -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    takePicture()
                } else {
                    Utils.showErrorSnack(snackbar_container, "Anda harus memberikan permission\nuntuk dapat menggunakan fitur ini")
                }
            }
            Constant.PERMISSION_REQ_CODE_CHAT -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    loadWebChatFragment()
                } else {
                    Utils.showErrorSnack(snackbar_container, "Anda harus memberikan permission\nuntuk dapat menggunakan fitur ini")
                }
            }
        }
    }

    private fun loadWebChatFragment() {
        Handler().postDelayed(
            {
                if (callerFragment!=null) {
                    (callerFragment as ChatFragment).loadWebChatFragment()
                }
                callerFragment = null
            }
            , 700
        )
    }

    private fun takePicture() {

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file: File = createPictureFile()

        val uri: Uri = FileProvider.getUriForFile(
            this,
            "com.bca.hallobca.fileprovider",
            file
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT,uri)
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1)
        
        startActivityForResult(intent, Constant.REQUEST_IMAGE_CAPTURE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constant.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            if (callerFragment != null) {
                // process callback here
                (callerFragment as ProfileFragment).setProfilePicFromBitmap(true, lastTakenPicPath)
                // then reset
                callerFragment = null
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun createPictureFile() : File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val filename = "JPEG_${timeStamp}"
        val fileext = ".jpg"

        val storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(filename, fileext, storageDir).apply {
            lastTakenPicPath = absolutePath
        }
    }

    fun requestCameraIntent(callerFragment: Fragment) {
        this.callerFragment = callerFragment
        if (checkPersmissionForCam()) takePicture() else requestPermissionForCam()
    }

    fun requestChatService(callerFragment: Fragment) {
        this.callerFragment = callerFragment
        if (checkPersmissionForChat()) loadWebChatFragment() else requestPermissionForChat()
    }

    private fun checkPersmissionForChat(): Boolean {
        return (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
            android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermissionForChat() {
        val permissions = arrayOf(
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        ActivityCompat.requestPermissions(this, permissions, Constant.PERMISSION_REQ_CODE_CHAT)
    }

    fun goHome() {
        navigation.selectedItemId = R.id.nav_home
        navigation.performClick()
    }

    fun setIsLogon(status: Boolean) {
        this.isLogon = status
        if (status) {
            goProfile()
        }
    }

    private fun goProfile() {
        navigation.selectedItemId = R.id.nav_profile
        navigation.performClick()
        this.isLogon = false
    }


}
