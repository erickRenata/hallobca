package com.bca.hallobca.ui.call

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ImageSpan
import android.view.View
import android.widget.RatingBar
import com.bca.hallobca.R
import com.bca.hallobca.data.model.home.MainIcon
import com.bca.hallobca.ui.home.HomeActivity
import com.bca.hallobca.utils.Constant
import com.bca.hallobca.utils.Utils
import kotlinx.android.synthetic.main.activity_review.*

class ReviewActivity : AppCompatActivity() {

    private var rating = 0.0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review)
        val currentCall = intent.getSerializableExtra(Constant.CALL_DEST) as? MainIcon

        call_icon.setImageResource(currentCall!!.icon!!)
        call_info.text = getString(R.string.call_screen_review_info, getString(currentCall.text!!))

        initEditText()
        addListenerOnRatingBar()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            rootLayout.systemUiVisibility = rootLayout.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.statusBarColor= getColor(R.color.colorWhite)
        }

        btn_send.setOnClickListener {
            if (inputValidated()) {
                startActivity(
                    Intent(this, HomeActivity::class.java)
                        .putExtra(Constant.ROLE_GUEST, false)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                )
                finish()
            }
        }
    }

    private fun initEditText() {
        val imageHint = ImageSpan(this, R.drawable.ic_pencil_edit)
        val imagePlaceholder = "       "
        val editHint = TextUtils.concat(imagePlaceholder, getString(R.string.call_screen_review_kritik_saran_hint))
        val spannableHint = SpannableString(editHint)
        spannableHint.setSpan(imageHint, 0, imagePlaceholder.length - 2, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE)
        kritik_saran.hint = spannableHint
    }

    private fun inputValidated() : Boolean {
        if (rating < 1.0) {
            Utils.showErrorSnack(rootLayout, getString(R.string.call_screen_review_no_rating))
            return false
        }
        if (rating < 4.0 && kritik_saran.text.isNullOrEmpty()) {
            Utils.showErrorSnack(rootLayout, getString(R.string.call_screen_review_no_entry))
            return false
        }
        return true
    }

    private fun addListenerOnRatingBar() {
        rating_bar.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener { _, rating, _ ->
            this.rating = rating
        }
    }

    override fun onBackPressed() {

    }
}
