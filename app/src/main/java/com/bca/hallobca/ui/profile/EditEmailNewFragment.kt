package com.bca.hallobca.ui.profile

import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.bca.hallobca.R
import com.bca.hallobca.utils.Utils
import kotlinx.android.synthetic.main.fragment_edit_email_new.*

class EditEmailNewFragment: Fragment() {
    private lateinit var screenError: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_edit_email_new, container, false)
        val nextButton = view.findViewById<Button>(R.id.button_continue)
        nextButton.setOnClickListener {
            hideScreenError()
            if (emailValidated()) {
                Utils.saveEmail(activity!!, input_new_email.text.toString())
                for (i in 1..activity!!.supportFragmentManager.backStackEntryCount) {
                    activity!!.supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                }
                val fragment: Fragment = EditEmailSuccessFragment()
                this.fragmentManager!!.beginTransaction()
                    .replace(R.id.content, fragment)
                    .addToBackStack(null)
                    .commit()
            }
        }
        nextButton.isEnabled = false

        val backButton = view.findViewById<TextView>(R.id.back_button)
        backButton.setOnClickListener {
            hideScreenError()
            activity!!.onBackPressed()
        }

        val inputEmail = view.findViewById<EditText>(R.id.input_old_email)
        inputEmail.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                hideScreenError()
                nextButton.isEnabled = true
                if (p0.isNullOrEmpty()) {
                    nextButton.isEnabled = false
                }
            }

        })

        screenError = view.findViewById(R.id.screen_error)
        return view
    }

    private fun emailValidated() : Boolean {
        val snackbarContainer = activity!!.findViewById<CoordinatorLayout>(R.id.snackbar_container)
        if (input_new_email.text.isNullOrEmpty()) {
//            Utils.showErrorSnack(snackbarContainer, activity!!.getString(R.string.error_old_email))
            showScreenError(activity!!.getString(R.string.error_old_email))
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(input_new_email.text).matches()) {
//            Utils.showErrorSnack(snackbarContainer, activity!!.getString(R.string.error_format_email))
            showScreenError(activity!!.getString(R.string.error_format_email))
            return false
        }
        return true
    }

    fun hideScreenError() {
        screenError.visibility = View.GONE
    }

    fun showScreenError(text: String) {
        screenError.text = text
        screenError.visibility = View.VISIBLE
    }
}