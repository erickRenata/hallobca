package com.bca.hallobca.ui.profile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bca.hallobca.R

class EditEmailSuccessFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_edit_email_success, container, false)
        val backButton = view.findViewById<TextView>(R.id.back_button)
        backButton.setOnClickListener {
            activity!!.onBackPressed()
        }
        return view
    }
}