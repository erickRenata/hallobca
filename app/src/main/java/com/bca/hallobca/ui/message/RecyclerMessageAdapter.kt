package com.bca.hallobca.ui.message

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bca.hallobca.R
import com.bca.hallobca.data.model.home.MessageItem
import kotlinx.android.synthetic.main.item_message.view.*

class RecyclerMessageAdapter(private val messageArray: ArrayList<MessageItem>) :
    RecyclerView.Adapter<RecyclerMessageAdapter.ItemHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_message, parent, false) as CardView
        return RecyclerMessageAdapter.ItemHolder(cardView)
    }

    override fun getItemCount() = messageArray.size

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        with (holder.cardView) {
            message_title.text = messageArray[position].messageTitle
            message_status.text = messageArray[position].messageStatus
        }
    }

    class ItemHolder(cardView: CardView) : RecyclerView.ViewHolder(cardView) {
        val cardView = cardView
    }
}