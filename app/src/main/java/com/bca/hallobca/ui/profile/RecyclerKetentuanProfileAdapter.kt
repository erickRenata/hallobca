package com.bca.hallobca.ui.profile

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.bca.hallobca.R

class RecyclerKetentuanProfileAdapter(private val ketentuanItemList : Array<String>) :
    RecyclerView.Adapter<RecyclerKetentuanProfileAdapter.ItemHolder>()  {

    class ItemHolder(cardView: CardView) : RecyclerView.ViewHolder(cardView) {
        val cardView = cardView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerKetentuanProfileAdapter.ItemHolder {
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_ketentuan_profile, parent, false) as CardView
        return RecyclerKetentuanProfileAdapter.ItemHolder(cardView)
    }

    override fun getItemCount() = ketentuanItemList.size

    override fun onBindViewHolder(holder: RecyclerKetentuanProfileAdapter.ItemHolder, position: Int) {
        holder.cardView.findViewById<TextView>(R.id.syarat_ketentuan_text).text = ketentuanItemList[position]
    }

}