package com.bca.hallobca.ui.history

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bca.hallobca.R
import com.bca.hallobca.data.model.home.HistoryItem

class HistoryFragment: Fragment() {
    private lateinit var recyclerHistoryAdapter: RecyclerView
    private lateinit var historyViewManager: RecyclerView.LayoutManager
    private lateinit var historyViewAdapter: RecyclerView.Adapter<*>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_histmsg, container, false)
        val titleText = view.findViewById<TextView>(R.id.fragment_title)
        titleText.text = activity!!.getString(R.string.history_fragment_title)
        val backButton = view.findViewById<TextView>(R.id.back_button)
        backButton.setOnClickListener {
            activity!!.onBackPressed()
        }

        val arrayHistory = ArrayList<HistoryItem>()

        historyViewManager = LinearLayoutManager(activity!!)
        historyViewAdapter = RecyclerHistoryAdapter(arrayHistory)

        recyclerHistoryAdapter = view.findViewById<RecyclerView>(R.id.list_container).apply {
            adapter = historyViewAdapter
            layoutManager = historyViewManager
        }

        return view
    }

}