package com.bca.hallobca.ui.register

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.bca.hallobca.R
import com.bca.hallobca.base.BaseActivity
import com.bca.hallobca.ui.otp.OtpActivity
import com.bca.hallobca.utils.Constant
import kotlinx.android.synthetic.main.activity_phone_select.*
import kotlinx.android.synthetic.main.popup_select_phone.select_phone_content

class PhoneSelectActivity: BaseActivity() {

    private lateinit var phoneListViewAdapter: RecyclerView.Adapter<*>
    private lateinit var phoneListViewManager: RecyclerView.LayoutManager
    private lateinit var bundle: Bundle
    private lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone_select)
        if (intent.extras == null) {
            startActivity(
                Intent(this, RegisterActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            finish()
        }

        prefs = this.getSharedPreferences(Constant.SP_GLOBAL, 0)

        bundle = intent.extras!!
        val phoneListString = bundle.getString(Constant.PHONE_NUMBER)
        val phoneList = phoneListString.split(",").toTypedArray()
        val crType = bundle.getInt(Constant.CR_TYPE)

        top_info_select_phone.text = getString(R.string.top_info_select_phone, if (crType>1) "kartu" else "rekening")
        phoneListViewManager = LinearLayoutManager(this)
        phoneListViewAdapter = RecyclerSelectPhoneAdapter(phoneList) { text: String, position: Int -> selectPhoneItemClicked(text, position)}
        select_phone_content.apply {
            layoutManager = phoneListViewManager
            adapter = phoneListViewAdapter
        }
        btn_masuk.isEnabled = false
        btn_masuk.setOnClickListener {
            startActivity(Intent(this, OtpActivity::class.java)
                .putExtras(bundle)
            )
        }
    }

    private fun selectPhoneItemClicked(text: String, position: Int) {
        btn_masuk.isEnabled = true
        (phoneListViewAdapter as RecyclerSelectPhoneAdapter).setSelectedItemPosition(position)
        bundle.putString(Constant.PHONE_NUMBER, text)
    }

}