package com.bca.hallobca.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.bca.hallobca.R
import com.bca.hallobca.base.BaseActivity
import com.bca.hallobca.ui.home.HomeActivity
import com.bca.hallobca.utils.Constant
import com.bca.hallobca.utils.Utils
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity: BaseActivity() {

    private var pinTries = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        back_button.setOnClickListener {
            onBackPressed()
        }

        input_pin.setOnTouchListener{v, event -> Utils.editTextEyeTouch(v as EditText, event)}
        Handler().postDelayed(
            {
                input_pin.requestFocus()
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(input_pin, InputMethodManager.SHOW_IMPLICIT)
            }
            , 200
        )
        button_start.setOnClickListener {
            button_start.requestFocus()
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(input_pin.windowToken, 0)
            if (pinValidated()) {
                startActivity(
                    Intent(this, HomeActivity::class.java)
                        .putExtra(Constant.ROLE_GUEST, false)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                )
                finish()
            } else {
                pinTries++
                if (pinTries<4) {
                    Utils.showErrorSnack(rootLayout, getString(R.string.error_invalid_pin, 4 - pinTries))
                } else {
                    Utils.showErrorSnack(rootLayout, getString(R.string.error_invalid_pin, 0))
                }
            }
        }
        input_pin.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (s!!.length==6) {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(input_pin.windowToken, 0)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

    }

    private fun pinValidated() : Boolean {
        if (input_pin.text.isNullOrEmpty()) {
            return false
        }
        val sepin = this.getSharedPreferences(Constant.SP_GLOBAL, 0)!!.getString(Constant.EPIN, "")
        val epin = Utils.hashPin(input_pin.text.toString())
        return sepin == epin
    }
}