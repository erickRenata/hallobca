package com.bca.hallobca.ui.chat

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.GridView
import android.widget.TextView
import com.bca.hallobca.R
import com.bca.hallobca.data.model.home.MainIcon
import com.bca.hallobca.ui.home.HomeActivity
import com.bca.hallobca.utils.CommonGridAdapter
import com.bca.hallobca.utils.Utils
import com.gdplabs.buzz.Application.BuzzConstant
import com.gdplabs.buzz.Application.Main.ChatActivity
//import com.gdplabs.buzz.Application.BuzzConstant
//import com.gdplabs.buzz.Application.Main.ChatActivity
import java.util.ArrayList

class ChatFragment: Fragment() {
    private lateinit var chatMenuAdapter: CommonGridAdapter
    private var menuList = ArrayList<MainIcon>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_chat, container, false)
        initChatMenu(view)

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        chatMenuAdapter = CommonGridAdapter(context, menuList, R.layout.item_chat_icon)

    }

    private fun initChatMenu(view: View) {
        menuList.clear()
        menuList.add(MainIcon(R.string.chat_icon_whatsapp, R.drawable.group_6))
        menuList.add(MainIcon(R.string.chat_icon_webchat, R.drawable.group_5))
        val grid = view.findViewById<GridView>(R.id.icon_container)
        grid.adapter = chatMenuAdapter
        grid.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            changeFragment(position)
        }
        val backButton = view.findViewById<TextView>(R.id.back_button)
        backButton.setOnClickListener {
            activity!!.onBackPressed()
        }

    }

    private fun changeFragment(position: Int) {
        when (position) {
            0 -> nagivateToWhatsapp("%23HaloBCA")
            1 -> (activity!! as HomeActivity).requestChatService(this)
        }
    }

    fun loadWebChatFragment() {
        val newFragment: Fragment? = if ((activity!! as HomeActivity).isGuest) ChatRegistrationFragment() else null
        if (newFragment == null) startChat()
        else {
            val fragment: Fragment = newFragment
            this.fragmentManager!!.beginTransaction()
                .replace(R.id.content, fragment)
                .addToBackStack(null)
                .commit()
        }
    }

    private fun nagivateToWhatsapp(message: String) {
        val i = Intent()
        i.putExtra(Intent.EXTRA_TEXT, message)
        i.action = Intent.ACTION_VIEW
        i.data = Uri.parse("https://wa.me/+628111500998?text=$message")
        startActivity(i)

    }

    private fun startChat() {
//        startActivity(Intent(context, ChatWaitingActivity::class.java))
        if (!(activity as HomeActivity).isGuest) {
            val intent = Intent(context, ChatActivity::class.java)
            intent.putExtra(BuzzConstant.BUNDLE_CUSTOMER_NAME_KEY, Utils.getUsername(activity!!))
            intent.putExtra(BuzzConstant.BUNDLE_CUSTOMER_EMAIL_KEY, Utils.getEmail(activity!!))
            intent.putExtra(BuzzConstant.BUNDLE_CUSTOMER_PHONE_NUMBER_KEY, Utils.getPhoneNumber(activity!!))
            intent.putExtra(BuzzConstant.BUNDLE_CUSTOMER_ACCESSED_FROM_KEY, "BCA Mobile")
            startActivity(intent)
        }
    }

}