package com.bca.hallobca.ui.register

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.bca.hallobca.R

class RecyclerKetentuanAdapter(private val ketentuanItemList : Array<String>) :
    RecyclerView.Adapter<RecyclerKetentuanAdapter.ItemHolder>() {

    class ItemHolder(cardView: CardView) : RecyclerView.ViewHolder(cardView) {
        val cardView = cardView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_ketentuan, parent, false) as CardView
        return ItemHolder(cardView)
    }

    override fun getItemCount() = ketentuanItemList.size

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.cardView.findViewById<TextView>(R.id.syarat_ketentuan_text).text = ketentuanItemList[position]
    }

}

