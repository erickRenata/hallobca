package com.bca.hallobca.ui.login

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.bca.hallobca.R
import com.bca.hallobca.ui.home.HomeActivity
import com.bca.hallobca.utils.Constant
import com.bca.hallobca.utils.Utils

class LoginFragment: Fragment() {
    private lateinit var errorText: TextView
    private lateinit var inputPin: EditText
    private var pinTries = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.activity_login, container, false)

        errorText = view.findViewById(R.id.error_text)
        val loginButton = view.findViewById<Button>(R.id.button_start)
        loginButton.isEnabled = false
        inputPin = view.findViewById(R.id.input_pin)
        inputPin.setOnTouchListener{v, event -> Utils.editTextEyeTouch(v as EditText, event)}
        inputPin.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                hideErrorText()
                if (s!!.length==6) {
                    loginButton.isEnabled = true
                    val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(inputPin.windowToken, 0)
                } else {
                    loginButton.isEnabled = false
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        Handler().postDelayed(
            {
                inputPin.requestFocus()
                val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(inputPin, InputMethodManager.SHOW_IMPLICIT)
            }
            , 200
        )
        loginButton.setOnClickListener {
            hideErrorText()
            if (pinValidated()) {
                (activity!! as HomeActivity).setIsLogon(true)
            } else {
                pinTries++
                if (pinTries<4) {
                    showErrorText(getString(R.string.error_invalid_pin, 4 - pinTries))
                } else {
                    //show popup blokir
                }
            }
        }
        val backButton = view.findViewById<TextView>(R.id.back_button)
        backButton.setOnClickListener {
            activity!!.onBackPressed()
        }

        return view
    }

    fun showErrorText(text: String) {
        errorText.text = text
        errorText.visibility = View.VISIBLE
    }

    fun hideErrorText() {
        errorText.text = ""
        errorText.visibility = View.GONE
    }

    private fun pinValidated() : Boolean {
        if (inputPin.text.isNullOrEmpty()) {
            return false
        }
        val sepin = activity!!.getSharedPreferences(Constant.SP_GLOBAL, 0)!!.getString(Constant.EPIN, "")
        val epin = Utils.hashPin(inputPin.text.toString())
        return sepin == epin
    }
}