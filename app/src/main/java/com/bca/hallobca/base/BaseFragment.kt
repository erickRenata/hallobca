package com.bca.hallobca.base

import android.app.ProgressDialog
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class BaseFragment : Fragment(){

    lateinit var mProgressDialog: ProgressDialog

    @get:LayoutRes
    abstract val layoutId : Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutId, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mProgressDialog = ProgressDialog(activity)
    }

    fun showLoading(){
        mProgressDialog.setCancelable(false)
        mProgressDialog.show()
    }

    fun showLoading(message: String){
        mProgressDialog.setMessage(message)
        mProgressDialog.setCancelable(false)
        mProgressDialog.show()
    }

    fun dismissLoading(){
        if (mProgressDialog.isShowing){
            mProgressDialog.dismiss()
        }
    }

}