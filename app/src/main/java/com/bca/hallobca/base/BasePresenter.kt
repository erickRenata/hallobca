package com.bca.hallobca.base

import com.bca.hallobca.data.network.ApiClient
import com.bca.hallobca.data.network.ApiService
import com.bca.hallobca.utils.rx.AppSchedulerProvider
import com.bca.hallobca.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BasePresenter<V> {

    var mvpView: V? = null
    lateinit var mApiService: ApiService
    lateinit var scheduler: SchedulerProvider
    val disposables = CompositeDisposable()

    fun launch(job: () -> Disposable) {
        disposables.add(job())
    }

    fun attachView(mvpView: V) {
        this.mvpView = mvpView
        scheduler = AppSchedulerProvider()
        mApiService = ApiClient.initRetrofit().create(ApiService::class.java)
    }

    fun detachView() {
        disposables.clear()
        this.mvpView = null
    }


}