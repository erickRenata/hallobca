package com.bca.hallobca.base

import android.os.Bundle

abstract class MvpActivity<P : BasePresenter<*>> : BaseActivity() {

    protected lateinit var mPresenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        mPresenter = createPresenter()
        super.onCreate(savedInstanceState)
    }

    protected abstract fun createPresenter(): P

    override fun onDestroy() {
        mPresenter.detachView()
        super.onDestroy()
    }
}