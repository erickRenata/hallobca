package com.bca.hallobca.base

import android.os.Bundle

abstract class MvpFragment<P : BasePresenter<*>> : BaseFragment() {

    protected lateinit var mPresenter: P

    protected abstract fun createPresenter(): P

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mPresenter = createPresenter()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }
}