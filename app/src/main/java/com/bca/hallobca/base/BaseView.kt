package com.bca.hallobca.base

interface BaseView {
    fun showLoading()
    fun dismissLoading()
    fun onFailed(message : String)
}