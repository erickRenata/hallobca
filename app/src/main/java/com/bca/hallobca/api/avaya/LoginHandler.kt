package com.bca.hallobca.api.avaya

import android.os.Bundle

interface LoginHandler {

    companion object {
        const val ERROR_CONNECTION_FAILED = -1
        const val ERROR_LOGIN_FAILED = -2
    }

    fun login(url: String, data: String, trustAllCerts: Boolean): Bundle
    fun login(url: String, data: String, trustAllCerts: Boolean, doAsPost: Boolean): Bundle
    fun logout(address: String)

}