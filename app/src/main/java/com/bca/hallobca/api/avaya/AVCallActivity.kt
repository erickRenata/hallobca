package com.bca.hallobca.api.avaya

import android.view.View

interface AVCallActivity {

    fun endCall(V: View)
    fun toggleMuteAudio(v: View)
    fun toggleEnableAudio(v: View)
    fun toggleMuteVideo(v: View)
    fun switchVideo(v: View)
    fun toggleEnableVideo(v: View)

}