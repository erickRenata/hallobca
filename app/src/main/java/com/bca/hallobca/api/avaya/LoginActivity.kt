package com.bca.hallobca.api.avaya

import android.view.View

interface LoginActivity {

    fun login(v: View)
}