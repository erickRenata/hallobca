package com.bca.hallobca.api.avaya

import android.view.View

interface AODialActivity {

    fun dialAudioOnly(v: View)
    fun logout(v: View)
}