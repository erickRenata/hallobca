package com.bca.hallobca.api.avaya

import android.view.View

interface AOCallActivity {

    fun endCall(v: View)
    fun toggleMute(v: View)
}