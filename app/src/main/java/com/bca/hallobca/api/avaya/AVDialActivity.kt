package com.bca.hallobca.api.avaya

import android.view.View

interface AVDialActivity {

    fun dialVideo(v: View)
    fun dialOneWayVideo(v: View)
    fun dialAudio(v: View)
    fun logout(v: View)
}