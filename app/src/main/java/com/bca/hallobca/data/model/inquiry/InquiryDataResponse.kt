package com.bca.hallobca.data.model.inquiry

data class InquiryDataResponse(val ListData: List<ListDataInquiryResponse>)

data class ListDataInquiryResponse(
    val PhoneID: String,
    val RegistrationDate: String,
    val CustomerNumber: String,
    val PhoneNumber: String,
    val Status: String,
    val LastUpdateDate: String,
    val ActivationDate: String,
    val Channel: String,
    val FINFlag: String,
    val FINDate: String,
    val FINUserUpdate: String,
    val Operator: String,
    val CustomerName: String
)