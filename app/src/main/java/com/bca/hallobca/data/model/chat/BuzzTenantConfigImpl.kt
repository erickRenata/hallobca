package com.bca.hallobca.data.model.chat

import com.gdplabs.buzz.Application.Config.BuzzTenantConfig
import id.gdplabs.ping.domain.common.Pair

class BuzzTenantConfigImpl : BuzzTenantConfig {

    override fun getTenantDomain(): String {
        // return Domain for User XMPP Halo BCA Chat
        return "chat.dti.co.id"
    }

    override fun getTenantAddress(): MutableList<Pair<String, Int>>? {
        // return List of Pair<String, Integer> which contains <Hostname Ejabberd, Connection XMPP over TCP Port>
        val listTenant = ArrayList<Pair<String, Int>> ()
        listTenant.add(Pair("devejb.wchat.bca.co.id", 443))
        return listTenant
    }

    override fun isConnectionSecured(): Boolean {
        return true
    }

    override fun getFileServerBaseUrl(): String {
        // return File Server URL for Halo BCA Chat
        return "http://devfs.wchat.bca.co.id/upload/"
    }

    override fun getFirebaseSenderID(): String {
        // return Firebase Sender ID for Halo BCA Chat
        return "456779935264"
    }

    override fun getTenantBuzzApiUrl(): String {
        // return Backend/API URL for Halo BCA Chat
        return "http://devbuz.wchat.bca.co.id/buzz-backend/"
    }

    override fun getTenantHostName(): String {
        // return Hostname Ejabberd Halo BCA Chat
        return "chat.dti.co.id"
    }

    override fun getFileDirectoryName(): String {
        // return Local File Directory Name (Folder to Save DownloadedImage)
        return ""
    }


}