package com.bca.hallobca.data.model.chat

class ChatItem {
    var message: String? = null
    var asSend: Boolean? = null

    constructor(message: String, asSend: Boolean) {
        this.message = message
        this.asSend = asSend

    }
}