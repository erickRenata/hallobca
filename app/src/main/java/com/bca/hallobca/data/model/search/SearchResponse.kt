package com.bca.hallobca.data.model.search

data class SearchResponse(
    val cis_individu: List<CisIndividu>
)

data class CisIndividu(
    val cis_customer_number: String,
    val cis_customer_type: String,
    val cis_last_update: CisLastUpdate,
    val customer_address: CustomerAddress,
    val customer_complement_data: CustomerComplementData,
    val customer_demographic_information: CustomerDemographicInformation,
    val customer_master_data: CustomerMasterData,
    val customer_name_and_phone: CustomerNameAndPhone,
    val customer_remark: CustomerRemark
)

data class CustomerAddress(
    val building: String,
    val city: String,
    val country: String,
    val district: String,
    val province: String,
    val rt: String,
    val rw: String,
    val street: String,
    val sub_district: String,
    val zip_code: String
)

data class CustomerRemark(
    val alert: String,
    val remark: String,
    val remark_effective_date: String,
    val remark_expired_date: String
)

data class CisLastUpdate(
    val officer_user_id: String,
    val update_date: String
)

data class CustomerNameAndPhone(
    val fax_phone: FaxPhone,
    val first_name: String,
    val full_name: String,
    val home_phone: HomePhone,
    val last_name: String,
    val middle_name: String,
    val office_phone: OfficePhone,
    val telex_number: String,
    val title: String
)

data class OfficePhone(
    val area_code: String,
    val country_code: String,
    val phone_number: String
)

data class FaxPhone(
    val area_code: String,
    val country_code: String,
    val fax_number: String
)

data class HomePhone(
    val area_code: String,
    val country_code: String,
    val phone_number: String
)

data class CustomerMasterData(
    val aeoi_assessable: String,
    val aeoi_form_flag: String,
    val birth_place: String,
    val branch_name: String,
    val branch_no: String,
    val cis_status: String,
    val citizenship: String,
    val citizenship_code: String,
    val close_date: String,
    val collectibility: String,
    val corporate_group: String,
    val credit_category: String,
    val customer_code: List<String>,
    val economic_sector: String,
    val email_advertisement: String,
    val fatca_assessable: String,
    val fatca_form_flag: String,
    val fatca_tax_no: String,
    val id_expired_date: String,
    val id_number: String,
    val mail_code: String,
    val open_date: String,
    val other_identity: OtherIdentity,
    val primary_officer: String,
    val provide_data_permission: String,
    val reference: String,
    val religion: String,
    val segmentation: String,
    val sex: String,
    val since_date: String,
    val sms_advertisement: String,
    val solicit: String,
    val tax_indicator: String,
    val tax_no: String,
    val telephone_advertisement: String
)

data class OtherIdentity(
    val id_number: String,
    val id_type: String
)

data class CustomerDemographicInformation(
    val birth_country_code: String,
    val birth_date: String,
    val death_date: String,
    val education: String,
    val house_ownership: String,
    val income: String,
    val marital_status: String,
    val mothers_name: String,
    val occupation: String
)

data class CustomerComplementData(
    val account_purpose: String,
    val business_sector: String,
    val company_address: List<String>,
    val company_name: String,
    val contact_information: ContactInformation,
    val income: String,
    val kitas: String,
    val kitas_expired_date: String,
    val position: String
)

data class ContactInformation(
    val building: String,
    val city: String,
    val country: String,
    val district: String,
    val email_address: String,
    val handphone: List<Handphone>,
    val province: String,
    val rt: String,
    val rw: String,
    val street: String,
    val sub_district: String,
    val zip_code: String
)

data class Handphone(
    val country_code: String,
    val phone_number: String
)