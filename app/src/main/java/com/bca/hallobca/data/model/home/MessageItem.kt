package com.bca.hallobca.data.model.home

class MessageItem {
    var messageTitle: String? = null
    var messageStatus: String? = null

    constructor(messageTitle: String, messageStatus: String) {
        this.messageTitle = messageTitle
        this.messageStatus = messageStatus
    }
}