package com.bca.hallobca.data.model.otp

data class VerifyOtpResponse(val OTPCode : String,
                                 val PAN : String,
                                 val Product : String,
                                 val PhoneNumber : String,
                                 val ReferenceCode : String,
                                 val Status : String,
                                 val AdditionalInfo : String?,
                                 val NumberOfActiveOTP : Int,
                                 val NumberOfActiveOTPInGroup : Int)