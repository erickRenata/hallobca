package com.bca.hallobca.data.model.otp

data class VerifyOtpRequest(val ClientId : String,
                            val PAN : String,
                            val Product : String,
                            val OTPCode : String)