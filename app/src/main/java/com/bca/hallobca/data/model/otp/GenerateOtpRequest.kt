package com.bca.hallobca.data.model.otp

data class GenerateOtpRequest(val ClientId : String,
                              val PAN : String,
                              val Product : String,
                              val ExpiredParam : Int?,
                              val AdditionalInfo : String?,
                              val PhoneNumber : String?,
                              val MessageContent : String?,
                              val MessageParameters : List<String>?)