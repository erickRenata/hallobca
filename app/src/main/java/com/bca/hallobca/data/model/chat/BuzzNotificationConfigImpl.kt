package com.bca.hallobca.data.model.chat

import android.content.Context
import android.content.Intent
import com.gdplabs.buzz.Application.Config.BuzzNotificationConfig
import com.gdplabs.buzz.Application.Main.ChatActivity

class BuzzNotificationConfigImpl : BuzzNotificationConfig {

    override fun getIntentMultipleNotification(p0: Context?): Intent {
        // return which Intent when notification clicked
        return Intent(p0, ChatActivity::class.java)
    }

    override fun getNotificationContentTitle(): String {
        // return Notification Title Name
        return "Halo BCA"
    }

    override fun getIntentSingleNotification(p0: Context?): Intent {
        // return which Intent when notification clicked
        return Intent(p0, ChatActivity::class.java)
    }

    override fun getNotificationParentStack(): Class<*> {
        return ChatActivity::class.java
    }

    override fun getNotificationContentSender(): String {
        // return Notification Sender Name
        return "Halo BCA"
    }
}