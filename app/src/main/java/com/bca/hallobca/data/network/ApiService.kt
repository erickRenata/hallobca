package com.bca.hallobca.data.network

import com.bca.hallobca.data.model.BaseResponse
import com.bca.hallobca.data.model.inquiry.InquiryDataResponse
import com.bca.hallobca.data.model.otp.*
import io.reactivex.Observable
import retrofit2.http.*
import com.bca.hallobca.data.model.otp.GenerateOtpRequest
import com.bca.hallobca.data.model.otp.GenerateOtpResponse
import com.bca.hallobca.data.model.otp.VerifyOtpRequest
import com.bca.hallobca.data.model.otp.VerifyOtpResponse
import com.bca.hallobca.data.model.search.SearchResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiService {

    /***** Profile *****/
    @GET("getName")
    fun getName(
        @Query("searchBy") searchBy: String,
        @Query("cardNumber") cardNumber: String
    ): Observable<String>

    @GET("getEmail")
    fun getEmail(
        @Query("searchBy") searchBy: String,
        @Query("cardNumber") cardNumber: String
    ): Observable<String>

    /***** OTP *****/
    @POST("")
    fun requestCode(@Body generateOtpRequest: GenerateOtpRequest): Observable<BaseResponse<GenerateOtpResponse>>

    @POST("")
    fun verifyCode(@Body verifyOtpRequest: VerifyOtpRequest): Observable<BaseResponse<VerifyOtpResponse>>

    /***** Inquiry Data *****/
    @FormUrlEncoded
    @POST("")
    fun inquiryData(
        @Query("phone-number") action: String,
        @Path("customerNumber") customerNumber: String
    ): Observable<BaseResponse<InquiryDataResponse>>

    /***** CISR Search *****/
    @GET("http://10.20.200.140:9405/cis/v2/search/db2")
    fun searchBy(
        @Query("search-by") searchBy: String,
        @Query("function-code") functionCode: Int,
        @Query("customer-name") customerName: String,
        @Query("birth-date") birthDate: String,
        @Query("gender") gender: String
    ): Observable<BaseResponse<SearchResponse>>
}