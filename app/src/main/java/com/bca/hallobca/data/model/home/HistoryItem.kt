package com.bca.hallobca.data.model.home

class HistoryItem {
    var historyIcon: Int? = null
    var historyTitle: String? = null
    var historyId: String? = null
    var historyDate: String? = null
    var historyTime: String? = null

    constructor(historyIcon: Int, historyTitle: String, historyId: String, historyDate: String, historyTime: String){
        this.historyIcon = historyIcon
        this.historyTitle = historyTitle
        this.historyId = historyId
        this.historyDate = historyDate
        this.historyTime = historyTime
    }
}