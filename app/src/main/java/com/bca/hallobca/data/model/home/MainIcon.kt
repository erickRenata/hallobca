package com.bca.hallobca.data.model.home

import java.io.Serializable

class MainIcon : Serializable {
    var text: Int? = null
    var icon: Int? = null

    constructor(text: Int, icon: Int) {
        this.text = text
        this.icon = icon

    }
    
}