package com.bca.hallobca.data.model.otp

import java.util.*

data class GenerateOtpResponse(val OTPCode : String,
                                   val PAN : String,
                                   val Product : String,
                                   val PhoneNumber : String,
                                   val ReferenceCode : String,
                                   val ExpiredParam : Int,
                                   val ExpiredDate : Date,
                                   val Status : String)