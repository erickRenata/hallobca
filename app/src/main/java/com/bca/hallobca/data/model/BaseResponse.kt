package com.bca.hallobca.data.model

open class BaseResponse<T> {
    var ErrorSchema : ErrorResponse? = null
    var OutputSchema : T? = null

    data class ErrorResponse(val ErrorCode : String,
                             val ErrorMessage : ErrorMessageResponse)

    data class ErrorMessageResponse(val English : String,
                                    val Indonesian : String)
}