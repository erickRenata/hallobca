package com.bca.hallobca.data.model.call

import android.content.Context
import com.avaya.clientplatform.api.AudioOnlyClientPlatform
import com.avaya.clientplatform.api.ClientPlatform
import com.avaya.clientplatform.api.ClientPlatformFactory

class ClientPlatformManager {

    companion object {
        var sClientPlatform: ClientPlatform? = null
        var sAudioOnlyClientPlatform: AudioOnlyClientPlatform? = null


        @Synchronized
        fun getClientPlatform(context: Context): ClientPlatform {
            if (sClientPlatform != null) {
                return sClientPlatform!!
            }
            sClientPlatform = ClientPlatformFactory.getClientPlatformInterface(context)
            return sClientPlatform!!
        }

        @Synchronized
        fun getAudioOnlyClientPlatform(context: Context): AudioOnlyClientPlatform {
            if (sAudioOnlyClientPlatform != null) {
                return sAudioOnlyClientPlatform!!
            }
            sAudioOnlyClientPlatform = ClientPlatformFactory.getAudioOnlyClientPlatformInterface(context)
            return sAudioOnlyClientPlatform!!
        }

    }
}