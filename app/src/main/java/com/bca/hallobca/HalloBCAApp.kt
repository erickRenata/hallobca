package com.bca.hallobca

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.bca.hallobca.data.model.chat.BuzzNotificationConfigImpl
import com.bca.hallobca.data.model.chat.BuzzTenantConfigImpl
import com.bca.hallobca.utils.chat.LocalStorageHelper
import com.gdplabs.buzz.Application.Buzz

class HalloBCAApp : Application() {
    override fun onCreate() {
        super.onCreate()

//        Hawk.init(applicationContext).build();
        Buzz.initialize(
            applicationContext,
            BuzzTenantConfigImpl(),
            BuzzNotificationConfigImpl(),
            LocalStorageHelper(this)
        )
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}